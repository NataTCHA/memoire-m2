# Mémoire M2

## Détection / Catégorisation de la parole pathologique

**Hypothèse** : Les patients atteints de SCA présentent une difficulté de rythme, c'est-à-dire un rythme plus lent et plus irrégulier entre les transitions de phonèmes. Il existe une plus grande difficulté pour les mouvements en arrière, tels que "go", "uj" et "badego", en raison des multiples cibles articulatoires.

## Plusieurs modèles linguistiques

### Annotation des séquences choisies manuellement
- **Stimuli** : "wiwiwi", "ajajaj", "ujujuj"

### Annotation des séquences choisies avec MFA
- **Stimuli DDK** : "bababa", "dedede", "gogogo", "badego"

### Modulation cepstrale
- Deux scripts :
  - `cepstral_modulation_example.py`
  - `lecture_resultats_coarticulation_pour_analyse.praat`
  - `lecture.py`

### Étude courbe d'intensité
- Identification des pics pour déterminer le nombre de voyelles

### Mesures
- `eventDur`
- `globDur`
- `meanch/mean`
- `posdyn/negdyn`
- `varco`
- `avg 7 maximum`
- `nb min/max`
- `nb pauses`
- `nb syllabes`

### Mesures retenues
- `eventdur`
- `std eventdur`
- `varco eventdur`
- `meanch`
- `std meanch`
- `avg pic max/min`
- `nb pic détecté`

### Conception fichier résultat
- Deux scripts avec en sortie un fichier TSV regroupant la totalité des mesures :
  - `mesure_seg.py` (méthode segmentale)
  - `count_peak_modulation.py` (méthode automatique)

### Conception graphiques
- Comparaison entre la méthode automatique et segmentale.
