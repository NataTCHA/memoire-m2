Métadonnées complètent ici :  https://docs.google.com/spreadsheets/d/1kxDdmot8SOiMujksiOMgIGanL_8T5LzrrmJJXwCdoRo/edit#gid=0

*groupe train et test utilisés pour les deux modèles*

**SCA**

Train
| SCA_ID      | Genre | Score | Sévérité       |
|-------------|-------|-------|----------------|
| SCA_H_AB02  | Homme | 6,5  | Moyennement sévère|
| SCA_F_AB24  | Femme | 1,4  | Pas sévère     ||
| SCA_F_AB23  | Femme | 7,6  | Moyennement sévère|
| SCA_H_AB14  | Homme | 3,4  | Pas sévère      |
| SCA_H_AB04  | Homme | 2,8  | Pas sévère  |
| SCA_F_AB32  | Femme | 4,5  | Pas sévère        |
| SNC_F_AB09  | Femme | 3  | Pas sévère    |
| SCA_F_AB29  | Femme | 8,8  | Moyennement sévère|
| SCA_F_AB22  | Femme | 7  | Moyennement sévère|
| SCA_H_AB34  | Homme | 6,4  | Moyennement sévère |
| SCA_F_AB19  | Femme | 17,2  | Très sévère|
| SCA_F_AB10  | Femme | 11,8  | Moyennement sévère|
| SCA_F_AB28  | Femme | 12,8  | Moyennement sévère|
| SCA_F_AB20  | Femme | 6,2  | Moyennement sévère|
| SCA_F_AB11  | Femme | 9,77  |Moyennement sévère|
| SCA_F_AB21  | Femme | 9,2  |Moyennement sévère|
| SCA_H_AB13  | Homme | 17  | Très sévère  |
| SCA_H_AB33  | Homme | 16,2  | Très sévère|
| SCA_F_AB27  | Femme | 6,8  | Moyennement sévère|
| SCA_H_AB18  | Homme | 11,5  |Moyennement sévère|
| SCA_H_AB26  | Homme | 4,3  | Pas sévère|
| SCA_F_AB03  | Femme | 5  | Pas Sévère  |
| SCA_F_AB08  | Femme | 8,1  | Moyennement Sévère  |

Test
| SCA_ID      | Genre | Score | Sévérité       |
|-------------|-------|-------|----------------|
| SCA_H_AB15  | Homme | 7,8  |Moyennement sévère|
| SCA_H_AB36  | Homme | 3,33  | Pas sévère|
| SCA_H_AB07  | Homme | 8  | Moyennement sévère|
| SCA_H_AB06  | Homme | 17  | Très Sévère         |
| SCA_H_AB37  | Homme | 10  | Moyennement sévère|
| SCA_H_AB17  | Homme | 15  | Très Sévère         |
| SCA_F_AB01  | Femme | 13,4  | Moyennement sévère|
| SCA_F_AB31  | Femme | 7  | Moyennement sévère|
| SCA_F_AB30  | Femme | 1,8  | Pas sévère     |
| SCA_H_AB25  | Homme | 5,6  | Pas sévère   |

**CTRL**

Test
| Locuteur   | Genre |
|------------|-------|
| SNC_F_AB01 | Femme |
| SNC_F_EC16 | Femme |
| SNC_F_SC3  | Femme |
| SNC_F_SC5  |Femme|
| SNC_H_AB3  | Homme |
| SNC_H_AB7  | Homme |
| SNC_H_ND4  | Homme |


Train
| Locuteur   | Genre |
|------------|-------|
| SNC_F_EC3  | Femme |
| SNC_F_EC4  | Femme |
| SNC_F_EC5  | Femme |
| SNC_F_EC14 | Femme |
| SNC_F_SC4  | Femme |
| SNC_F_SC6  | Femme |
| SNC_F_VJ09 | Femme |
| SNC_H_AB08 | Homme |
| SNC_H_EC2  | Homme |
| SNC_H_EC8  | Homme |
| SNC_H_EC7  | Homme |
| SNC_H_EC9  | Homme |
| SNC_H_FB14 | Homme |
| SNC_H_MB3  | Homme |
| SNC_H_MB7  | Homme |
| SNC_H_MJ10 | Homme |
| SNC_H_MJ25 | Homme |
| SNC_H_MJ26 | Homme |
| SNC_H_ND6  | Homme |
| SNC_H_ND8  | Homme |
| SNC_H_ND9  | Homme |
| SNC_H_SC1  | Homme |

**Pour bandes modulation cepstrale** : 

85 % accuracy 

![Matrice de confusion bandes glides](matric_confusion.png)

| Image Name                                                     | Actual Class | Predicted Class |
|----------------------------------------------------------------|--------------|------------------|
| SCA_F_AB30_2020_11_26_ModuleTransitions_aj1.txt                | malade       | non_malade       |
| SCA_F_AB30_2020_11_26_ModuleTransitions_uj4.txt                | malade       | non_malade       |
| SCA_F_AB30_2020_11_26_ModuleTransitions_wi4.txt                | malade       | non_malade       |
| SCA_F_AB31_2020_11_26_ModuleTransitions_aj3.txt                | malade       | non_malade       |
| SCA_H_AB25_2020_11_13_ModuleTransitions_aj2.txt                | malade       | non_malade       |
| SCA_H_AB25_2020_11_13_ModuleTransitions_aj3.txt                | malade       | non_malade       |
| SCA_H_AB37_2020_02_11_ModuleTransitions_wi3.txt                | malade       | non_malade       |
| SNC_F_AB1_2019_11_07_ModuleTransitions_aj4.txt                 | non_malade   | malade           |
| SNC_F_AB1_2019_11_07_ModuleTransitions_wi1.txt                 | non_malade   | malade           |
| SNC_F_AB1_2019_11_07_ModuleTransitions_wi2.txt                 | non_malade   | malade           |
| SNC_F_EC16_2019_12_13_ModuleTransitions_uj1.txt               | non_malade   | malade           |
| SNC_F_SC3_2019_11_07_ModuleTransitions_aj1.txt                 | non_malade   | malade           |
| SNC_F_SC3_2019_11_07_ModuleTransitions_aj2.txt                 | non_malade   | malade           |
| SNC_F_SC3_2019_11_07_ModuleTransitions_uj1.txt                 | non_malade   | malade           |
| SNC_F_SC3_2019_11_07_ModuleTransitions_uj2.txt                 | non_malade   | malade           |
| SNC_F_SC3_2019_11_07_ModuleTransitions_uj4.txt                 | non_malade   | malade           |
| SNC_F_SC5_2019_11_14_ModuleTransitions_aj1.txt                 | non_malade   | malade           |
| SNC_F_SC5_2019_11_14_ModuleTransitions_wi2.txt                 | non_malade   | malade           |
| SNC_H_AB3_2019_11_20_ModuleTransitions_uj3.txt                 | non_malade   | malade           |
| SNC_H_AB3_2019_11_20_ModuleTransitions_wi1.txt                 | non_malade   | malade           |
| SNC_H_AB3_2019_11_20_ModuleTransitions_wi3.txt                 | non_malade   | malade           |
| SNC_H_AB7_2020_03_03_ModuleTransitions_uj3.txt                 | non_malade   | malade           |
| SNC_H_AB7_2020_03_03_ModuleTransitions_wi1.txt                 | non_malade   | malade           |
| SNC_H_AB7_2020_03_03_ModuleTransitions_wi3.txt                 | non_malade   | malade           |
| SNC_H_AB7_2020_03_03_ModuleTransitions_wi4.txt                 | non_malade   | malade           |
| SNC_H_ND4_2020_01_31_ModuleTransitions_uj3.txt                | non_malade   | malade           |


**Mal Classé:**


![Graphiques en barre stimuli mal classé par groupe  de sévérité](locmaladae_bandes.png)
**Mal Classé (Par glides):**
![Graphiques en barre stimuli mal classé par groupe SCA, SNC](stimuli_occurence.png)


**Pour Spectrogramme**

79 % accuracy

![Matrice de confusion spectro glides](matric_confusion_spectro.png)

| Image Name                                                     | Actual Class | Predicted Class |
|----------------------------------------------------------------|--------------|------------------|
| spectrogram_SCA_F_AB01_2020_03_05_ModuleTransitions_wi3_wiwiwi_0.png | malade   | non_malade       |
| spectrogram_SCA_F_AB30_2020_11_26_ModuleTransitions_aj1_ajajaj_0.png | malade   | non_malade       |
| spectrogram_SCA_F_AB30_2020_11_26_ModuleTransitions_aj4_ajajaj_0.png | malade   | non_malade       |
| spectrogram_SCA_F_AB30_2020_11_26_ModuleTransitions_uj3_ujujuj_0.png | malade   | non_malade       |
| spectrogram_SCA_F_AB31_2020_11_26_ModuleTransitions_aj2_ajajaj_0.png | malade   | non_malade       |
| spectrogram_SCA_H_AB15_2020_11_10_ModuleTransitions_aj4_ajajaj_0.png | malade   | non_malade       |
| spectrogram_SCA_H_AB15_2020_11_10_ModuleTransitions_uj1_ujujuj_0.png | malade   | non_malade       |
| spectrogram_SCA_H_AB15_2020_11_10_ModuleTransitions_uj2_ujujuj_0.png | malade   | non_malade       |
| spectrogram_SCA_H_AB36_2020_02_04_ModuleTransitions_aj1_ajajaj_0.png | malade   | non_malade       |
| spectrogram_SCA_H_AB36_2020_02_04_ModuleTransitions_aj4_ajajaj_0.png | malade   | non_malade       |
| spectrogram_SCA_H_AB36_2020_02_04_ModuleTransitions_uj3_ujujuj_0.png | malade   | non_malade       |
| spectrogram_SCA_H_AB36_2020_02_04_ModuleTransitions_wi3_wiwiwi_0.png | malade   | non_malade       |
| spectrogram_SCA_H_AB36_2020_02_04_ModuleTransitions_wi4_wiwiwi_0.png | malade   | non_malade       |
| spectrogram_SCA_H_AB37_2020_02_11_ModuleTransitions_aj1_ajajaj_0.png | malade   | non_malade       |
| spectrogram_SCA_H_AB37_2020_02_11_ModuleTransitions_wi1_wiwiwi_0.png | malade   | non_malade       |
| spectrogram_SCA_H_AB37_2020_02_11_ModuleTransitions_wi2_wiwiwi_0.png | malade   | non_malade       |
| spectrogram_SCA_H_AB37_2020_02_11_ModuleTransitions_wi3_wiwiwi_0.png | malade   | non_malade       |
| spectrogram_SCA_H_AB37_2020_02_11_ModuleTransitions_wi4_wiwiwi_0.png | malade   | non_malade       |
| spectrogram_SNC_F_AB1_2019_11_07_ModuleTransitions_aj2_ajajaj_0.png | non_malade | malade         |
| spectrogram_SNC_F_AB1_2019_11_07_ModuleTransitions_uj4_ujujuj_0.png | non_malade | malade         |
| spectrogram_SNC_F_AB1_2019_11_07_ModuleTransitions_wi3_wiwiwi_0.png | non_malade | malade         |
| spectrogram_SNC_F_AB1_2019_11_07_ModuleTransitions_wi4_wiwiwi_0.png | non_malade | malade         |
| spectrogram_SNC_F_EC16_2019_12_13_ModuleTransitions_aj1_ajajaj_0.png | non_malade | malade         |
| spectrogram_SNC_F_EC16_2019_12_13_ModuleTransitions_aj2_ajajaj_0.png | non_malade | malade         |
| spectrogram_SNC_H_AB3_2019_11_20_ModuleTransitions_aj1_ajajaj_0.png | non_malade | malade         |
| spectrogram_SNC_H_AB3_2019_11_20_ModuleTransitions_aj2_ajajaj_0.png | non_malade | malade         |
| spectrogram_SNC_H_AB3_2019_11_20_ModuleTransitions_aj3_ajajaj_0.png | non_malade | malade         |
| spectrogram_SNC_H_AB3_2019_11_20_ModuleTransitions_aj4_ajajaj_0.png | non_malade | malade         |
| spectrogram_SNC_H_AB3_2019_11_20_ModuleTransitions_uj1_ujujuj_0.png | non_malade | malade         |
| spectrogram_SNC_H_AB3_2019_11_20_ModuleTransitions_uj3_ujujuj_0.png | non_malade | malade         |
| spectrogram_SNC_H_AB3_2019_11_20_ModuleTransitions_uj4_ujujuj_0.png | non_malade | malade         |
| spectrogram_SNC_H_AB3_2019_11_20_ModuleTransitions_wi1_wiwiwi_0.png | non_malade | malade         |
| spectrogram_SNC_H_AB3_2019_11_20_ModuleTransitions_wi3_wiwiwi_0.png | non_malade | malade         |
| spectrogram_SNC_H_AB3_2019_11_20_ModuleTransitions_wi4_wiwiwi_0.png | non_malade | malade         |
| spectrogram_SNC_H_AB7_2020_03_03_ModuleTransitions_uj1_ujujuj_0.png | non_malade | malade         |
| spectrogram_SNC_H_AB7_2020_03_03_ModuleTransitions_uj2_ujujuj_0.png | non_malade | malade         |
| spectrogram_SNC_H_AB7_2020_03_03_ModuleTransitions_uj3_ujujuj_0.png | non_malade | malade         |

**Mal Classés:**

![Graphiques en barre stimuli mal classé par groupe de sévérité](locmaladae.png)
**Mal Classé (Par glides):**
![Graphiques en barre stimuli mal classé par groupe SCA, SNC](stimuli_occurence_spectro.png)