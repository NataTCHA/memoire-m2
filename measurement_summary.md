**EVENTDUR(STD/VARCO)**
- Prend la durée de la réalisation d'un maximum

**POSDYN**: Mesure la pente positive
  - Soit durée entre minimum[i] -> minimum[i+1]
  - Entre le minimum et le maximum qui suit

**GLOBDUR**
- Prend la durée totale de la portion annotée

**MEANCH(STD)**: "Valeur moyenne du first minimum (segment 1 steady-state) to the last minimum (segment 6 steady-state)"
  - Soit durée entre minimum[i] -> minimum[dernier indice)
  - De la portion annotée

**NEGDYN**: Mesure la pente négative entre le maximum et le minimum qui suit

### Additional Measures
**val_moyenne_X_pic**: 

Tableaux reprenant les valeurs r,mesures orthogonales des deux modèles
https://docs.google.com/spreadsheets/d/1OL276bT852BlTnGkThkkIPokdMo7ka75lLbOIMYjuNA/edit?usp=sharing