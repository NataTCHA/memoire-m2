import tkinter as tk
from tkinter import filedialog, ttk, messagebox
from PIL import Image, ImageTk
import matplotlib.pyplot as plt
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg, NavigationToolbar2Tk
from pydub import AudioSegment
from librosa.feature import mfcc
from librosa.core import load as audioLoad
import numpy as np
import os
from scipy import signal
import tgt

from matplotlib.colors import ListedColormap
class AudioSignalViewer:
    def happy_theme(self):
        style = ttk.Style()
        style.configure(".", background="#FFEB3B", foreground="black", font=('Helvetica', 10))
        style.configure("TFrame", background="#FFC107")
        style.configure("TLabel", background="#FFEB3B", foreground="black")
        style.configure("TButton", padding=(10, 5), font=('Helvetica', 10), background="#4CAF50", foreground="black")

    def __init__(self, master):
        self.master = master
        self.master.title("Audio Signal Viewer")
        self.master.geometry("1500x800")

        self.happy_theme()

        self.menu_bar = tk.Menu(self.master)
        self.file_menu = tk.Menu(self.menu_bar, tearoff=0)
        self.file_menu.add_command(label="Ouvrir un fichier audio", command=self.open_file)
        self.file_menu.add_command(label="Quitter", command=self.master.destroy)
        self.menu_bar.add_cascade(label="Fichier", menu=self.file_menu)

        self.view_menu = tk.Menu(self.menu_bar, tearoff=0)
        self.view_menu.add_command(label="Réinitialiser le graphique", command=self.reset_graph)
        self.view_menu.add_command(label="Afficher Bande MFCC", command=self.show_mfcc_legend_band)
        self.view_menu.add_command(label="Afficher Spectrogramme", command=self.show_spectrogram)
        self.menu_bar.add_cascade(label="Affichage", menu=self.view_menu)

        self.about_menu = tk.Menu(self.menu_bar, tearoff=0)
        self.about_menu.add_command(label="À propos", command=self.show_about_info)
        self.menu_bar.add_cascade(label="À propos", menu=self.about_menu)

        self.master.config(menu=self.menu_bar)

        background_image = Image.open("background_image.png")
        photo = ImageTk.PhotoImage(background_image)
        background_label = tk.Label(master, image=photo)
        background_label.image = photo
        background_label.place(relwidth=1, relheight=1)

        icon_image = Image.open("background_image.png")
        icon_photo = ImageTk.PhotoImage(icon_image)
        master.iconphoto(False, icon_photo)

        self.label_file_name = ttk.Label(self.master, text="", font=("Helvetica", 14), background="#F0F0F0")
        self.label_file_name.pack(side=tk.TOP, pady=10)

        self.btn_open = ttk.Button(self.master, text="Ouvrir un fichier audio", command=self.open_file)
        self.btn_open.pack(pady=10)

        self.measurements_frame = ttk.Frame(self.master, padding=(10, 10, 10, 10), style="TFrame")
        self.measurements_frame.pack(side=tk.RIGHT)

        self.figure, self.ax = plt.subplots(figsize=(5, 3), tight_layout=True)
        self.canvas = FigureCanvasTkAgg(self.figure, master=self.master)
        self.canvas.get_tk_widget().pack(side=tk.LEFT, padx=10, pady=10)

        toolbar = NavigationToolbar2Tk(self.canvas, self.master)
        toolbar.update()
        self.canvas.get_tk_widget().pack(side=tk.LEFT, padx=10, pady=10)
        self.canvas._tkcanvas.pack(side=tk.TOP, fill=tk.BOTH, expand=1)

        self.label_value = ttk.Label(self.measurements_frame, text="", style="TLabel")
        self.label_value.grid(row=0, column=0, pady=5)

        self.label_coordinates = ttk.Label(self.measurements_frame, text="Coordonnées enregistrées:", style="TLabel")
        self.label_coordinates.grid(row=1, column=0, pady=5)

        self.label_distances = ttk.Label(self.measurements_frame, text="Distances entre les pics:", style="TLabel")
        self.label_distances.grid(row=2, column=0, pady=5)

        self.label_average_distance = ttk.Label(self.measurements_frame, text="Moyenne des distances:", style="TLabel")
        self.label_average_distance.grid(row=3, column=0, pady=5)

        self.btn_annotation = ttk.Button(self.master, text="Afficher Annotation", command=self.show_annotation)
        self.btn_annotation.pack(pady=10)

        self.clicked_points = []
        self.minima_distances = []
        self.sum_at_clicked_points = 0
        self.clicked_points_count = 0

        self.audio_file_path = ""
        self.sig_sr = 0

        self.figure.canvas.mpl_connect('button_press_event', self.on_click)

        self.happy_theme()

    def open_file(self):
        file_path = filedialog.askopenfilename(title="Sélectionner un fichier audio", filetypes=[("Fichiers audio", "*.wav;*.mp3")])

        if file_path:
            self.get_MFCCS_change(file_path)
            file_name = os.path.basename(file_path)
            self.label_file_name.config(text=f"Fichier audio : {file_name}")

    def on_click(self, event):
        if self.is_zoom_tool_active():
            return

        x, y = event.xdata, event.ydata
        self.clicked_points.append((x, y))

        self.ax.annotate('X', xy=(x, y), xytext=(0, -5), textcoords="offset points", ha='center', va='bottom')
        self.canvas.draw()

        self.label_value.config(text=f"Coordonnées du clic : x={x:.4f}, y={y:.4f}")
        coordinates_text = "\n".join([f"({x:.4f}, {y:.4f})" for x, y in self.clicked_points])
        self.label_coordinates.config(text=f"Coordonnées enregistrées:\n{coordinates_text}")

        if x is not None and y is not None:
            self.sum_at_clicked_points += y
            self.clicked_points_count += 1
            average_value = self.sum_at_clicked_points / self.clicked_points_count
            self.label_value.config(text=f"Coordonnées du clic : x={x:.4f}, y={y:.4f}\nMoyenne : {average_value:.4f}")

            if len(self.clicked_points) >= 2:
                times = np.array([point[0] for point in self.clicked_points])
                minima_distances_interval = self.calculate_distances(times)
                self.minima_distances.extend(minima_distances_interval)
                distances_text = "\n".join([f"{distance:.4f}" for distance in minima_distances_interval])
                self.label_distances.config(text=f"Distances entre les pics:\n{distances_text}")
                average_distance = np.mean(self.minima_distances)
                self.label_average_distance.config(text=f"Moyenne des distances : {average_distance:.4f}")

    def reset_graph(self):
        self.ax.clear()
        self.canvas.draw()
        self.clicked_points = []
        self.minima_distances = []
        self.sum_at_clicked_points = 0
        self.clicked_points_count = 0
        self.label_value.config(text="")
        self.label_coordinates.config(text="Coordonnées enregistrées:")
        self.label_distances.config(text="Distances entre les pics:")
        self.label_average_distance.config(text="Moyenne des distances :")

    def calculate_distances(self, times):
        distances = np.diff(times)
        return distances

    def get_MFCCS_change(self, file_path, channelN=0, sigSr=10000, tStep=0.005, winLen=0.025, n_mfcc=13, n_fft=512, removeFirst=1, filtCutoff=12, filtOrd=6):
        self.audio_file_path = file_path
        self.sig_sr = sigSr

        try:
            myAudio, _ = audioLoad(file_path, sr=sigSr, mono=False)

            if len(np.shape(myAudio)) > 1:
                y = myAudio[channelN, :]
            else:
                y = myAudio

            win_length = np.rint(winLen * sigSr).astype(int)
            hop_length = np.rint(tStep * sigSr).astype(int)

            myMfccs = mfcc(y=y, sr=sigSr, n_mfcc=n_mfcc, win_length=win_length, hop_length=hop_length, n_fft=n_fft)

            T = np.round(np.multiply(np.arange(1, np.shape(myMfccs)[1] + 1), tStep) + winLen / 2, 4)

            if removeFirst:
                myMfccs = myMfccs[1:, :]

            cutOffNorm = filtCutoff / ((1 / tStep) / 2)
            b1, a1 = signal.butter(filtOrd, cutOffNorm, 'lowpass')

            filtMffcs = signal.filtfilt(b1, a1, myMfccs)

            myAbsDiff = np.sqrt(np.gradient(filtMffcs, axis=1) ** 2)

            totChange = np.sum(myAbsDiff, 0)

            totChange = signal.filtfilt(b1, a1, totChange)

            self.modulation_mfcc_plot(totChange)

        except Exception as e:
            print(f"Error loading audio file: {e}")

    def modulation_mfcc_plot(self, valeurs):
        fs = 200
        temps = np.arange(0, len(valeurs)) / fs

        self.ax.clear()
        self.ax.plot(temps, valeurs, label='Modulation MFCC', color="#FF5733")
        self.ax.set(title='Modulation MFCC', xlabel='Temps', ylabel='Valeur de modulation')
        self.ax.legend()

        self.canvas.draw()

    def show_annotation(self):
        file_path = filedialog.askopenfilename(title="Sélectionner un fichier TextGrid", filetypes=[("Fichiers TextGrid", "*.TextGrid")])

        if file_path:
            text_grid = tgt.io.read_textgrid(file_path)
            tier_name = "words"
            tier = None
            for t in text_grid.tiers:
                if t.name == tier_name:
                    tier = t
                    break

            if tier is not None:
                intervals = tier.intervals
                word_coordinates = []

                for interval in intervals:
                    start_time = interval.start_time
                    end_time = interval.end_time
                    word_coordinates.append((start_time, end_time))

                self.update_graph_with_word_coordinates(word_coordinates)

    def update_graph_with_word_coordinates(self, word_coordinates):
        for start_time, end_time in word_coordinates:
            self.ax.axvline(x=start_time, color='#4286f4', linestyle='--', label='Word Interval Start')
            self.ax.axvline(x=end_time, color='#f44174', linestyle='--', label='Word Interval End')

        self.ax.legend()
        self.canvas.draw()

    def update_graph(self, audio_segment):
        self.ax.clear()
        self.ax.plot(np.arange(len(audio_segment)) / self.sig_sr, audio_segment, label='Signal Audio', color="#4286f4")
        self.ax.set(title='Signal Audio', xlabel='Temps', ylabel='Amplitude')
        self.ax.legend()
        self.canvas.draw()
    
    def show_spectrogram(self):
            if not self.audio_file_path:
                messagebox.showwarning("Avertissement", "Veuillez d'abord ouvrir un fichier audio.")
                return

            try:
                myAudio, _ = audioLoad(self.audio_file_path, sr=self.sig_sr, mono=False)

                if len(np.shape(myAudio)) > 1:
                    y = myAudio[0, :]
                else:
                    y = myAudio

                f, t, Sxx = signal.spectrogram(y, fs=self.sig_sr, nperseg=1024, noverlap=512, nfft=2048)

                cmap_plasma = plt.get_cmap('plasma')
                new_cmap = ListedColormap(cmap_plasma(np.linspace(1, 0.2, cmap_plasma.N)))  
                plt.figure(figsize=(12, 6))
                plt.pcolormesh(t, f, 10 * np.log10(Sxx), shading='gouraud', cmap=new_cmap)
                plt.title('Spectrogramme')
                plt.xlabel('Temps (s)')
                plt.ylabel('Fréquence (Hz)')
                plt.colorbar(label='Amplitude (dB)')
                plt.tight_layout()

                self.show_spectrogram_in_tkinter(plt)

            except Exception as e:
                messagebox.showerror("Erreur", f"Une erreur s'est produite : {e}")


    def show_spectrogram_in_tkinter(self, plt):
        spectrogram_window = tk.Toplevel(self.master)
        spectrogram_window.title("Spectrogramme")

        canvas = FigureCanvasTkAgg(plt.gcf(), master=spectrogram_window)
        canvas.draw()
        canvas.get_tk_widget().pack(side=tk.TOP, fill=tk.BOTH, expand=1)

            # Ajout de la barre de navigation
        toolbar = NavigationToolbar2Tk(canvas, spectrogram_window)
        toolbar.update()
        canvas.get_tk_widget().pack(side=tk.TOP, fill=tk.BOTH, expand=1)
        canvas.mpl_connect("close_event", lambda event: spectrogram_window.destroy())




    def is_zoom_tool_active(self):
        if hasattr(self.canvas, "toolbar"):
            for child in self.canvas.toolbar.winfo_children():
                if "Zoom" in str(child):
                    return True
        return False

    def show_about_info(self):
        about_text = "Audio Signal Viewer\nVersion 1.0\n\n© 2023 Natou"
        tk.messagebox.showinfo("À propos", about_text)

    def show_mfcc_legend_band(self):
        if not self.audio_file_path:
            messagebox.showwarning("Avertissement", "Veuillez d'abord ouvrir un fichier audio.")
            return

        try:
            self.get_MFCCS_change(self.audio_file_path)
            self.show_mfcc_legend_band_without_normalization()

        except Exception as e:
            messagebox.showerror("Erreur", f"Une erreur s'est produite : {e}")

    def show_mfcc_legend_band_without_normalization(self):
        mfcc_values = self.calculate_mfcc_values()

        fig, ax = plt.subplots(figsize=(300, 1))
        ax.imshow([mfcc_values], cmap='gray_r', aspect='auto', vmin=4, vmax=60)
        ax.set_axis_off()


    def calculate_mfcc_values(self):
        myAudio, _ = audioLoad(self.audio_file_path, sr=self.sig_sr, mono=False)

        if len(np.shape(myAudio)) > 1:
            y = myAudio[0, :]
        else:
            y = myAudio

        win_length = np.rint(0.025 * self.sig_sr).astype(int)
        hop_length = np.rint(0.005 * self.sig_sr).astype(int)

        myMfccs = mfcc(y=y, sr=self.sig_sr, n_mfcc=13, win_length=win_length, hop_length=hop_length, n_fft=512)
        avg_mfcc_values = np.mean(myMfccs, axis=1)
        return avg_mfcc_values


if __name__ == "__main__":
    root = tk.Tk()
    app = AudioSignalViewer(root)
    root.mainloop()
