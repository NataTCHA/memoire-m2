
import pandas as pd
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import PolynomialFeatures
from sklearn.metrics import accuracy_score, confusion_matrix, classification_report

# Charger les données depuis le fichier TSV
data = pd.read_csv('model1_def.tsv', delimiter='\t')

# Créer une fonction pour mapper les scores en catégories
def categorize_score(score):
    if score < 6:
        return 'Faible'
    elif score < 14:
        return 'Modéré'
    else:
        return 'Élevé'

# Appliquer la fonction pour obtenir les catégories de score
data['Score_Cat'] = data['Score'].apply(categorize_score)

# Sélectionner les colonnes nécessaires pour la prédiction
X = data[['globdur',"new_measure"]]
y = data['Score_Cat']

# Créer un transformateur de caractéristiques pour générer des interactions entre les mesures
poly = PolynomialFeatures(degree=2, include_bias=False)
X_poly = poly.fit_transform(X)

# Créer un modèle de régression logistique multinomiale
model = LogisticRegression(multi_class='multinomial', solver='lbfgs')

# Scinder les données en ensembles d'entraînement et de test
X_train, X_test, y_train, y_test = train_test_split(X_poly, y, test_size=0.3, random_state=42)

# Entraîner le modèle sur l'ensemble d'entraînement
model.fit(X_train, y_train)

# Faire des prédictions sur l'ensemble de test
y_pred = model.predict(X_test)

# Calculer la précision du modèle
accuracy = accuracy_score(y_test, y_pred)
print(f"Précision du modèle : {accuracy * 100:.2f}%")

# Afficher la matrice de confusion et le rapport de classification
confusion = confusion_matrix(y_test, y_pred)
classification_rep = classification_report(y_test, y_pred)
print("Matrice de confusion :\n", confusion)
print("\nRapport de classification :\n", classification_rep)