import parselmouth
from praatio import tgio
import matplotlib.pyplot as plt
from scipy.interpolate import interp1d
import numpy as np
import os
from scipy.signal import savgol_filter

def process_and_save_image(sound_file, textgrid_file, output_folder):
    sound = parselmouth.Sound(sound_file)
    tg = tgio.openTextgrid(textgrid_file)
    phone_tier = tg.tierDict['words']
    first_interval = phone_tier.entryList[0]
    start_time, end_time = first_interval[0], first_interval[1]
    extract_s = sound.extract_part(from_time=start_time, to_time=end_time)
    formants = extract_s.to_formant_burg()
    time_values = formants.ts()
    formant1_dict = {}
    
    for time_point in time_values:
        formant1_value = formants.get_value_at_time(formant_number=1, time=time_point)
        formant2_value = formants.get_value_at_time(formant_number=2, time=time_point)
        formant3_value = formants.get_value_at_time(formant_number=3, time=time_point)

        formant1_dict[time_point] = formant1_value

    preserved_formant1_dict = {}

    time_step = 0.005

    for i in range(len(time_values)):
        current_time = time_values[i]

        next_time_index = np.argmin(np.abs(np.array(time_values) - (current_time + time_step)))
        if next_time_index < len(time_values):
            next_time = time_values[next_time_index]
            diff_formant1 = abs(formant1_dict[next_time] - formant1_dict[current_time])
            if diff_formant1 <= 100:
                preserved_formant1_dict[current_time] = formant1_dict[current_time]

    interp_func1 = interp1d(list(preserved_formant1_dict.keys()), list(preserved_formant1_dict.values()), kind='linear')
    time_values_interp1 = np.linspace(min(preserved_formant1_dict.keys()), max(preserved_formant1_dict.keys()), num=1000)
    smoothed_curve1 = savgol_filter(interp_func1(time_values_interp1), window_length=101, polyorder=1)

    pixel_intensity = np.abs(smoothed_curve1)
    pixel_intensity_normalized = (pixel_intensity - pixel_intensity.min()) / (pixel_intensity.max() - pixel_intensity.min())

    fig, ax = plt.subplots(figsize=(300, 1))
    plt.imshow([pixel_intensity_normalized], cmap='gray', aspect='auto')
    ax.set_axis_off()

    output_file = os.path.join(output_folder, os.path.basename(sound_file).replace(".wav", "_pixel_intensity.png"))
    plt.savefig(output_file, bbox_inches='tight', pad_inches=0)
    plt.close()

sca_folder = "SCA/glides"
ctrl_folder = "CTRL/glides"
output_sca_folder = "output/sca" 
output_ctrl_folder = "output/ctrl" 

os.makedirs(output_sca_folder, exist_ok=True)
os.makedirs(output_ctrl_folder, exist_ok=True)

for folder, output_folder in [(sca_folder, output_sca_folder), (ctrl_folder, output_ctrl_folder)]:
    for root, dirs, files in os.walk(folder):
        for file_name in files:
            if file_name.endswith(".wav"):
                sound_file = os.path.join(root, file_name)
                textgrid_file = os.path.join(root, file_name.replace(".wav", ".TextGrid"))
                
                process_and_save_image(sound_file, textgrid_file, output_folder)
