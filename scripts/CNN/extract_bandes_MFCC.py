import os
import numpy as np
import matplotlib.pyplot as plt
from praatio import tgio

def get_mfcc_values(mfcc_filename, textgrid_filename):
    with open(mfcc_filename, 'r') as f:
        valeurs_mfcc = [float(line.strip()) for line in f]

    tg = tgio.openTextgrid(textgrid_filename)
    start_time, end_time = tg.tierDict["'Segmentation'"].entryList[0][0], tg.tierDict["'Segmentation'"].entryList[-1][1]
    start_index = int(start_time * 200) 
    end_index = int(end_time * 200)

    portion_mfcc = valeurs_mfcc[start_index:end_index]
    return portion_mfcc

def normalize_mfcc_values(portion_mfcc, global_min, global_max):
    normalized_values = [(x - global_min) / (global_max - global_min) for x in portion_mfcc]
    return normalized_values

sca_folder = "SCA/DDK_4sec"
ctrl_folder = "CTRL/DDK_4sec"

global_min = float('inf')
global_max = float('-inf')

for folder in [sca_folder, ctrl_folder]:
    for root, dirs, files in os.walk(folder):
        for file_name in files:
            if file_name.endswith(".txt"):
                mfcc_file_path = os.path.join(root, file_name)
                
                textgrid_file_path = os.path.join(root, file_name.replace(".txt", ".TextGrid"))
                
                portion_mfcc = get_mfcc_values(mfcc_file_path, textgrid_file_path)
                local_min = min(portion_mfcc)
                local_max = max(portion_mfcc)
                
                if local_min < global_min:
                    global_min = local_min
                    file_min = file_name
                
                if local_max > global_max:
                    global_max = local_max
                    file_max = file_name
                
                print(f"Fichier: {file_name}, Min: {local_min}, Max: {local_max}")

print(f"Valeur Min Globale: {global_min} (Fichier: {file_min})")
print(f"Valeur Max Globale: {global_max} (Fichier: {file_max})")

for folder in [sca_folder, ctrl_folder]:
    for root, dirs, files in os.walk(folder):
        for file_name in files:
            if file_name.endswith(".txt"):
                mfcc_file_path = os.path.join(root, file_name)
                textgrid_file_path = os.path.join(root, file_name.replace(".txt", ".TextGrid"))
                
                portion_mfcc = get_mfcc_values(mfcc_file_path, textgrid_file_path)
                normalized_values = normalize_mfcc_values(portion_mfcc, global_min, global_max)
                
                fig, ax = plt.subplots(figsize=(300, 1))  
                ax.imshow([normalized_values], cmap='gray_r', aspect='auto')
                ax.set_axis_off()

                output_folder = "malade_ddk" if "SCA" in mfcc_file_path else "sain_ddk"

                output_path = os.path.join(output_folder, f"{file_name}_output_image.png")
                plt.savefig(output_path, bbox_inches='tight', pad_inches=0)

                plt.close()
