import numpy as np
import matplotlib.pyplot as plt
import tgt
import os

def find_values_in_time_interval(filename, tier_name, fs, output_dir):
    with open(filename, 'r') as f:
        valeurs_modulation = [float(line.strip()) for line in f]

    valeurs = np.array(valeurs_modulation)
    duration = len(valeurs) / fs
    time = np.linspace(0, duration, len(valeurs))

    textgrid_filename = filename.replace('.txt', '.TextGrid')
    textgrid = tgt.io.read_textgrid(textgrid_filename)

    tier = textgrid.get_tier_by_name(tier_name)

    for interval in tier.intervals:
        start_time = interval.start_time
        end_time = interval.end_time
        start_sample = int(start_time * fs)
        end_sample = int(end_time * fs)

        modulation_values = valeurs[start_sample:end_sample]

        plt.figure()
        plt.plot(modulation_values,color='purple')
        plt.axis('off')
        
        image_file = os.path.join(output_dir, f"modulation_{start_time}-{end_time}.png")
        plt.savefig(image_file)
        plt.close()

#test
filename = "SCA_F_AB11_2020_10_29_ModuleTransitions_aj1.txt"
tier_name = "seq"
fs = 200  
output_dir = ""
find_values_in_time_interval(filename, tier_name, fs, output_dir)
