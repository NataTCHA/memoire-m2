import os
import parselmouth
import numpy as np
import matplotlib.pyplot as plt
from praatio import tgio
import glob


def export_grayscale_spectrogram_with_zero_padding(spectrogram, filename, image_width_inches, image_height_inches, resolution_dpi=100, dynamic_range=160, zero_padding_width_ratio=0, value_for_zero_padding=None):
    X, Y = spectrogram.x_grid(), spectrogram.y_grid()
    sg_db = 10 * np.log10(spectrogram.values)

    if(zero_padding_width_ratio > 0 and zero_padding_width_ratio < 1):
        if(value_for_zero_padding is None):
            value_for_zero_padding = np.min(sg_db)
        n_rows = sg_db.shape[0]
        n_cols = sg_db.shape[1]
        n_added_cols = int(np.round(n_cols * zero_padding_width_ratio / (1 - zero_padding_width_ratio)))
        added_zeros = np.ones((n_rows, n_added_cols)) * value_for_zero_padding
        zero_padded_sg_db = np.flipud(np.concatenate((sg_db, added_zeros), axis=1))
    else:
        zero_padded_sg_db = np.flipud(sg_db)

    fig = plt.figure(frameon=False)
    fig.set_size_inches(image_width_inches, image_height_inches)
    ax = plt.Axes(fig, [0., 0., 1., 1.])
    ax.set_axis_off()
    fig.add_axes(ax)
    ax.imshow(zero_padded_sg_db, aspect="auto", cmap="gray_r", vmin=sg_db.max() - dynamic_range, vmax=None)
    plt.savefig(filename, bbox_inches='tight', pad_inches=0, dpi=resolution_dpi)
    plt.close()

def extract_vowel_spectrograms(audio_file, textgrid_file, output_folder, dynamic_range):
    sound = parselmouth.Sound(audio_file)
    tg = tgio.openTextgrid(textgrid_file)
    phon_tier = tg.tierDict['words']

    for idx, interval in enumerate(phon_tier.entryList):
        label = interval.label
        if label.lower() in ['ajajaj', 'ujujuj', 'wiwiwi','bababa',"badegobadegobadgeo","dedede","gogogo"]:
            start_time = interval.start
            end_time = interval.end

            extract_s = sound.extract_part(from_time=start_time, to_time=end_time)

            max_frequency_spectrogram_Hz = 8000
            spectrogram = extract_s.to_spectrogram(maximum_frequency=max_frequency_spectrogram_Hz)

            audio_file_name = os.path.splitext(os.path.basename(audio_file))[0]
            output_filename = f"{output_folder}/spectrogram_{audio_file_name}_{label}_{idx}.png"

            export_grayscale_spectrogram_with_zero_padding(
                spectrogram, filename=output_filename,
                image_width_inches=8, image_height_inches=6, dynamic_range=110)

def process_patient_folder(patient_folder, output_folder, dynamic_range):
    for root, dirs, files in os.walk(patient_folder):
        for file in files:
            if file.lower().endswith(".wav"):
                audio_file = os.path.join(root, file)
                textgrid_file = os.path.join(root, file.replace(".wav", ".TextGrid"))

                if os.path.exists(textgrid_file):
                    extract_vowel_spectrograms(audio_file, textgrid_file, output_folder, dynamic_range)

def main():
    input_folder = 'glides/'
    output_folder = 'test'
    dynamic_range = 120  

    if not os.path.exists(output_folder):
        os.makedirs(output_folder)

    for patient_folder in glob.glob(os.path.join(input_folder, '*/')):
        process_patient_folder(patient_folder, output_folder, dynamic_range)

if __name__ == "__main__":
    main()
