import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import classification_report
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import SimpleRNN, LSTM, GRU, Bidirectional, Dense

ctrl_data = pd.read_csv("CTRL_DDK3.tsv", delimiter='\t')
sca_data = pd.read_csv("SCA_DDK3.tsv", delimiter='\t')

ctrl_data['classe'] = 0
sca_data['classe'] = 1

data = pd.concat([ctrl_data, sca_data], ignore_index=True)
features = data[['event-dur', 'std_meanch', 'meanch', "std-eventdur", "avg_seg"]]
labels = data['classe']
X_train, X_test, y_train, y_test = train_test_split(features, labels, test_size=0.2, random_state=42)

scaler = StandardScaler()
X_train_scaled = scaler.fit_transform(X_train)
X_test_scaled = scaler.transform(X_test)

X_train_rnn = np.reshape(X_train_scaled, (X_train_scaled.shape[0], 1, X_train_scaled.shape[1]))
X_test_rnn = np.reshape(X_test_scaled, (X_test_scaled.shape[0], 1, X_test_scaled.shape[1]))

#SimpleRNN model
model_simple_rnn = Sequential()
model_simple_rnn.add(SimpleRNN(50, input_shape=(1, X_train_scaled.shape[1]), activation='relu'))
model_simple_rnn.add(Dense(1, activation='sigmoid'))
model_simple_rnn.compile(optimizer='adam', loss='binary_crossentropy', metrics=['accuracy'])
model_simple_rnn.fit(X_train_rnn, y_train, epochs=25, batch_size=32, validation_split=0.2, verbose=2)
loss_simple_rnn, accuracy_simple_rnn = model_simple_rnn.evaluate(X_test_rnn, y_test)

#LSTM model
model_lstm = Sequential()
model_lstm.add(LSTM(50, input_shape=(1, X_train_scaled.shape[1]), activation='relu'))
model_lstm.add(Dense(1, activation='sigmoid'))
model_lstm.compile(optimizer='adam', loss='binary_crossentropy', metrics=['accuracy'])
model_lstm.fit(X_train_rnn, y_train, epochs=25, batch_size=32, validation_split=0.2, verbose=2)
loss_lstm, accuracy_lstm = model_lstm.evaluate(X_test_rnn, y_test)

#GRU model
model_gru = Sequential()
model_gru.add(GRU(50, input_shape=(1, X_train_scaled.shape[1]), activation='relu'))
model_gru.add(Dense(1, activation='sigmoid'))
model_gru.compile(optimizer='adam', loss='binary_crossentropy', metrics=['accuracy'])
model_gru.fit(X_train_rnn, y_train, epochs=25, batch_size=32, validation_split=0.2, verbose=2)
loss_gru, accuracy_gru = model_gru.evaluate(X_test_rnn, y_test)

#Bidirectional LSTM model
model_bidirectional_lstm = Sequential()
model_bidirectional_lstm.add(Bidirectional(LSTM(50, activation='relu'), input_shape=(1, X_train_scaled.shape[1])))
model_bidirectional_lstm.add(Dense(1, activation='sigmoid'))
model_bidirectional_lstm.compile(optimizer='adam', loss='binary_crossentropy', metrics=['accuracy'])
model_bidirectional_lstm.fit(X_train_rnn, y_train, epochs=25, batch_size=32, validation_split=0.2, verbose=2)
loss_bidirectional_lstm, accuracy_bidirectional_lstm = model_bidirectional_lstm.evaluate(X_test_rnn, y_test)

print(f"SimpleRNN Model - Accuracy: {accuracy_simple_rnn * 100:.2f}%")
print(f"LSTM Model - Accuracy: {accuracy_lstm * 100:.2f}%")
print(f"GRU Model - Accuracy: {accuracy_gru * 100:.2f}%")
print(f"Bidirectional LSTM Model - Accuracy: {accuracy_bidirectional_lstm * 100:.2f}%")
