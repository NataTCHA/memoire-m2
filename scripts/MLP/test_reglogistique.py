import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score, classification_report

ctrl_data = pd.read_csv("CTRL_glides.tsv", delimiter='\t') 
sca_data = pd.read_csv("SCA_glides.tsv", delimiter='\t')  

ctrl_data['classe'] = 0
sca_data['classe'] = 1

data = pd.concat([ctrl_data, sca_data], ignore_index=True)

features = data[['event-dur', 'std_meanch', 'meanch', 'std-eventdur','avg_seg']]
labels = data['classe']

X_train, X_test, y_train, y_test = train_test_split(features, labels, test_size=0.2, random_state=42)

scaler = StandardScaler()
X_train_scaled = scaler.fit_transform(X_train)
X_test_scaled = scaler.transform(X_test)
logistic_regression = LogisticRegression(random_state=42)

logistic_regression.fit(X_train_scaled, y_train)

predictions = logistic_regression.predict(X_test_scaled)

accuracy = accuracy_score(y_test, predictions)
print(f"Précision du modèle : {accuracy * 100:.2f}%")
class_report = classification_report(y_test, predictions, target_names=['Non Malade (0)', 'Malade (1)'])

print("Rapport de Classification :")
print(class_report)