#from get_MFCCS_change import get_MFCCS_change



# -*- coding: utf-8 -*-
"""
Created on Thu Jun  9 08:45:25 2022

@author: Leonardo
"""

import os
import glob
import numpy as np
from librosa.feature import mfcc
from librosa.core import load as audioLoad
from scipy import signal



def get_MFCCS_change(filePath=None, channelN=0, sigSr=10000, tStep=0.005, winLen=0.025, n_mfcc=13, n_fft=512, removeFirst=1, filtCutoff=12, filtOrd=6):
    """ computes the amount of change in the MFCCs over time
    
    INPUT:
        
    filePath(default="./signals/audio/loc01_pert1_1_2_3_6_01.wav"
    channelN (default(default=0): selet the channel number for multichannel audio file
    sigSr (default=10000): frequency at which resampling the audio for the analysis
    tStep (default=0.005): analysis time step in ms
    winLen (default=0.025): analysis window length in ms
    n_mfcc (default=13): number of MFCCs to compute (the first one may then be removed via reoveFirst)
    n_fft (default=512): number of points for the FFT
    removeFirst (default=1): if one the first cepstral corefficient is dicarded
    filtCutoff (default=12): bandpass fitler freq.in Hz
    filtOrd (default=6): bandpass filter order
    
    OUTPUT:
    
    totChange: Amount of change over time
    T: time stamps for each value   
    """
    
    myAudio, _ = audioLoad(filePath,sr=sigSr, mono=False)
    
    if len(np.shape(myAudio))>1:
        y=myAudio[channelN,:]
    else:
        y=myAudio
    
    win_length=np.rint(winLen*sigSr).astype(int)
    hop_length=np.rint(tStep*sigSr).astype(int)
    
    
    myMfccs=mfcc( y=y, sr=sigSr, n_mfcc=n_mfcc, win_length=win_length, hop_length=hop_length,n_fft=n_fft)
    
    T=np.round(np.multiply(np.arange(1,np.shape(myMfccs)[1]+1),tStep)+winLen/2,4)
    
    if removeFirst:
       myMfccs=myMfccs[1:,:]
       
    cutOffNorm = filtCutoff / ((1/tStep) / 2)
    
    b1,a1 =signal.butter(filtOrd,cutOffNorm, 'lowpass')
    
    filtMffcs = signal.filtfilt(b1,a1,myMfccs)
    
    myAbsDiff=np.sqrt(np.gradient(filtMffcs,axis=1)**2)
    
    totChange=np.sum(myAbsDiff,0)
    
    totChange=signal.filtfilt(b1,a1,totChange)
    
    return totChange, T
    #plt.plot(totChange)

dossier_principal = "pourNatacha"

def parcourir_arborescence_dossier(dossier):
    """
    Parcourt l'arborescence du dossier spécifié et applique la fonction get_MFCCS_change() sur les fichiers .wav.
    Enregistre les résultats dans des fichiers .txt avec le même nom que les fichiers .wav.
    """
    for dossier_actuel, sous_dossiers, fichiers in os.walk(dossier):
        for fichier in fichiers:
            if fichier.endswith(".wav"):
                chemin_fichier = os.path.join(dossier_actuel, fichier)
                print("Chemin du fichier .wav :", chemin_fichier)
                spectralChange, timeStamps = get_MFCCS_change(chemin_fichier)
                nom_fichier_resultat = os.path.splitext(fichier)[0] + ".txt"
                chemin_fichier_resultat = os.path.join(dossier_actuel, nom_fichier_resultat)
                np.savetxt(chemin_fichier_resultat, spectralChange)

parcourir_arborescence_dossier(dossier_principal)

#print(0)
#print(np)