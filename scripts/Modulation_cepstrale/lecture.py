import numpy as np
from scipy.io import wavfile
import os

def modulation_mfcc(fichier_mfcc):
    with open(fichier_mfcc, 'r') as f:
        valeurs_mfcc = [float(line.strip()) for line in f]

    valeurs = np.array(valeurs_mfcc)

    # Paramètres audio
    fs = 200  # Fréquence d'échantillonnage en Hz

    # Remove .txt extension from filename
    nom_fichier_wav = os.path.splitext(fichier_mfcc)[0] + "_modulation.Sound"
    wavfile.write(nom_fichier_wav, fs, valeurs.astype(np.float64))
    return nom_fichier_wav





def parcourir_arborescence_dossier(dossier):
    """
    Parcourt l'arborescence du dossier spécifié et applique la fonction modulation_mfcc() sur les fichiers .txt.
    Enregistre les résultats dans des fichiers .Sound avec le nom de fichiers fichiertxt_modulation.Sound.
    """
    for dossier_actuel, sous_dossiers, fichiers in os.walk(dossier):
        for fichier in fichiers:
            if fichier.endswith(".txt"):
                chemin_fichier = os.path.join(dossier_actuel, fichier)        
                modulation = modulation_mfcc(chemin_fichier)
                print("modulation cepstrale :", modulation)

dossier_principal = "pourNatacha"
parcourir_arborescence_dossier(dossier_principal)
