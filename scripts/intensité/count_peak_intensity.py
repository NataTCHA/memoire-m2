import parselmouth
import numpy as np
import matplotlib.pyplot as plt
from scipy.ndimage import gaussian_filter1d
from scipy.signal import find_peaks
import tgt
from scipy.signal import savgol_filter
import csv
    
import os

# Chemin vers le dossier racine de l'arborescence
root_dir = 'DDK_2'
# Chemin vers le fichier de sortie
output_file_path = 'resultats.tsv'
# Parcourir les sous-dossiers de DDK
with open(output_file_path, 'x', newline='') as output_file:
    # Créer un objet writer pour écrire dans le fichier TSV
    tsv_writer = csv.writer(output_file, delimiter='\t')
    
    # Écrire l'en-tête du fichier
    tsv_writer.writerow(['Nom du fichier', 'Nombre de pics'])
    for dirpath, dirnames, filenames in os.walk(root_dir):
        for filename in filenames:
            if filename.endswith('.wav'):
                audio_file = os.path.join(dirpath, filename)
                txt_grid = os.path.splitext(audio_file)[0] + '.TextGrid'
                
                if os.path.exists(txt_grid):
                    def extract_label_windows(file_path, tier_name="words"):
                        text_grid = tgt.io.read_textgrid(file_path)
                        tier = None
                        
                        for t in text_grid.tiers:
                            if t.name == tier_name:
                                tier = t
                                break

                        label_windows = []
                        if tier is not None and len(tier.intervals) > 0:
                            for interval in tier.intervals:
                                start_time = interval.start_time
                                end_time = interval.end_time




                                label_windows.append(start_time)
                                label_windows.append(end_time)

                        return label_windows

                    fenetre=extract_label_windows(txt_grid,tier_name="words")

                    sound = parselmouth.Sound(audio_file)
                    spectrogram = sound.to_spectrogram()
                    intensity = sound.to_intensity()

                    freqs = np.array(spectrogram.y_grid())

                    times = np.array(intensity.xs())  

                    start_time = fenetre[0]  # en secondes
                    end_time = fenetre[1]  # en secondes

                    start_idx = np.argmin(np.abs(times - start_time))
                    end_idx = np.argmin(np.abs(times - end_time))

                    # Lisser la courbe d'intensité en utilisant un filtre gaussien
                    sigma = 3.5 # Paramètre de lissage du filtre gaussien
                    smoothed_intensity = gaussian_filter1d(intensity.values.T[start_idx:end_idx], sigma=sigma, axis=0)


                    peaks, _ = find_peaks(smoothed_intensity[:, 0], prominence=0.5)  
                    nombre_de_pics = len(peaks)
                    print("Nombre de pics détectés :", nombre_de_pics)


                    tsv_writer.writerow([audio_file, nombre_de_pics])

                pass
print("Les résultats ont été enregistrés dans", output_file_path)
