import parselmouth
import numpy as np
import matplotlib.pyplot as plt
from scipy.ndimage import gaussian_filter1d
from scipy.signal import find_peaks

audio_file = "SCA_H_AB14_2020_11_10_ModuleRepetition_max_bababa_max.wav"
sound = parselmouth.Sound(audio_file)
spectrogram = sound.to_spectrogram()
intensity = sound.to_intensity()

freqs = np.array(spectrogram.y_grid())

times = np.array(intensity.xs())  

start_time = 2.9561  # en secondes
end_time = 3.358919  # en secondes

start_idx = np.argmin(np.abs(times - start_time))
end_idx = np.argmin(np.abs(times - end_time))

# Lisser la courbe d'intensité en utilisant un filtre gaussien
sigma = 3.5  # Paramètre de lissage du filtre gaussien
smoothed_intensity = gaussian_filter1d(intensity.values.T[start_idx:end_idx], sigma=sigma, axis=0)


peaks, _ = find_peaks(smoothed_intensity[:, 0], prominence=0.1)  


plt.figure(figsize=(12, 8))

plt.subplot(3, 1, 1)
plt.imshow(spectrogram.as_array(), origin='lower', aspect='auto', cmap='inferno', extent=[spectrogram.xmin, spectrogram.xmax, freqs.min(), freqs.max()])
plt.colorbar(format='%+2.0f dB')
plt.title('Spectrogramme')
plt.ylabel('Fréquence (Hz)')

plt.subplot(3, 1, 2)
plt.plot(times[start_idx:end_idx], intensity.values.T[start_idx:end_idx], label='Intensité')
plt.xlabel('Temps (s)')
plt.ylabel('Intensité (db)')
plt.title('Courbe d\'intensité')
plt.legend()

plt.subplot(3, 1, 3)
plt.plot(times[start_idx:end_idx], smoothed_intensity, label='Intensité filtrée', color='orange')
plt.plot(times[start_idx:end_idx][peaks], smoothed_intensity[peaks], 'ro', label='Maxima locaux')
plt.xlabel('Temps (s)')
plt.ylabel('Intensité (db)')
plt.title('Courbe d\'intensité filtrée')
plt.legend()

plt.tight_layout()
plt.show()


peak_times = times[start_idx:end_idx][peaks]
peak_intensities = smoothed_intensity[peaks]
for t, intensity in zip(peak_times, peak_intensities):
    print("Temps du maximum local:", t)
    print("Intensité du maximum local:", intensity)
    print("--------")
