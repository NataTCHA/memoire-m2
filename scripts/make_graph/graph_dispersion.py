import matplotlib.pyplot as plt
import pandas as pd

df_ctrl = pd.read_csv("auto/CTRL_glides.tsv", sep='\t')
df_sca = pd.read_csv("auto/SCA_glides.tsv", sep='\t')
df_auto = pd.read_csv("auto/SCA_glides_auto.tsv", sep='\t')
df_seg = pd.read_csv("auto/CTRL_glides_auto.tsv", sep='\t')

unique_words = df_ctrl["mot"].unique()

x_min = min(df_ctrl["event-dur"].min(), df_sca["event-dur"].min(), df_auto["event-dur"].min(), df_seg["event-dur"].min())
x_max = max(df_ctrl["event-dur"].max(), df_sca["event-dur"].max(), df_auto["event-dur"].max(), df_seg["event-dur"].max())
y_min = min(df_ctrl["meanch"].min(), df_sca["meanch"].min(), df_auto["meanch"].min(), df_seg["meanch"].min())
y_max = max(df_ctrl["meanch"].max(), df_sca["meanch"].max(), df_auto["meanch"].max(), df_seg["meanch"].max())

color_map = {'CTRL': 'orange', 'SCA': 'red', 'AUTO': 'green', 'SEG': 'blue'}

num_plots = len(unique_words)
num_cols = 2
num_rows = (num_plots - 1) // num_cols + 1

fig, axes = plt.subplots(num_rows, num_cols, figsize=(12, 8))
fig.tight_layout(pad=3.0)

for idx, word in enumerate(unique_words):
    row_idx, col_idx = divmod(idx, num_cols)
    ax = axes[row_idx, col_idx]

    subset_ctrl = df_ctrl[df_ctrl["mot"] == word]
    ax.scatter(
        subset_ctrl["event-dur"],
        subset_ctrl["meanch"],
        label='CTRL segmenté',
        color=color_map['CTRL'],
        marker='^',  # Triangle marker
        alpha=0.7,  # Transparency
    )

    subset_sca = df_sca[df_sca["mot"] == word]
    ax.scatter(
        subset_sca["event-dur"],
        subset_sca["meanch"],
        label='SCA segmenté',
        color=color_map['SCA'],
        marker='^',  # Triangle marker
        alpha=0.7,  # Transparency
    )

    subset_auto = df_auto[df_auto["mot"] == word]
    ax.scatter(
        subset_auto["event-dur"],
        subset_auto["meanch"],
        label='SCA automatique',
        color=color_map['AUTO'],
        marker='o',  # Circle marker
        alpha=0.7,  # Transparency
    )

    subset_seg = df_seg[df_seg["mot"] == word]
    ax.scatter(
        subset_seg["event-dur"],
        subset_seg["meanch"],
        label='CTRL automatique',
        color=color_map['SEG'],
        marker='o',  # Circle marker
        alpha=0.7,  # Transparency
    )

    ax.set_xlim(x_min, x_max)
    ax.set_ylim(y_min, y_max)

    ax.set_xlabel("Event Duration")
    ax.set_ylabel("Meanch")
    ax.set_title(word)
    ax.legend()
plt.show()
