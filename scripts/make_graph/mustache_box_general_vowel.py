import tgt
import os
import numpy as np
from scipy.signal import argrelextrema
import seaborn as sns
import pandas as pd
import matplotlib.pyplot as plt

def find_maxima_in_interval(values, start_time, end_time):
    fs = 200
    duration = len(values) / fs
    time = np.linspace(0, duration, len(values))


    indices = np.where((time >= start_time) & (time <= end_time))[0]
    interval_values = values[indices]

    maxima_indices = argrelextrema(interval_values, np.greater)[0]

    if len(maxima_indices) > 1:
        max_index = indices[maxima_indices[np.argmax(interval_values[maxima_indices])]]
        max_value = interval_values[maxima_indices[np.argmax(interval_values[maxima_indices])]]
        return max_index, max_value
    elif len(maxima_indices) == 1:
        max_index = indices[maxima_indices[0]]
        max_value = interval_values[maxima_indices[0]]
        return max_index, max_value
    else:
        average = np.mean(interval_values)

        return None, average


def extract_label_windows(file_path, tier_name="phon"):
    text_grid = tgt.io.read_textgrid(file_path)
    tier = None

    for t in text_grid.tiers:
        if t.name == tier_name:
            tier = t
            break

    label_windows = []
    if tier is not None:
        previous_label = None
        
        for interval in tier.intervals:
            start_time = interval.start_time
            end_time = interval.end_time

            if previous_label is None:  # Premier label
                xmin = start_time - 0.50
                xmax = (start_time + end_time) / 2
                labels = [interval.text]
                label_windows.append((xmin, xmax, labels))
            else:  # Label suivant
                xmin = (previous_label.start_time + previous_label.end_time) / 2
                xmax = (start_time + end_time) / 2
                labels = [interval.text]
                label_windows.append((xmin, xmax, labels))

            previous_label = interval

        if previous_label is not None:  # Dernier label
            start_time = previous_label.start_time
            end_time = previous_label.end_time
            xmin = (start_time + end_time) / 2
            xmax = end_time + 0.10
            labels = []
            label_windows.append((xmin, xmax, labels))

    return label_windows

root_folder_path = "pourNatacha"

locuteur_folders = os.listdir(root_folder_path)

locuteur_labels = []
locuteur_data = []
locuteur_names = []

for locuteur_folder in locuteur_folders:
    locuteur_folder_path = os.path.join(root_folder_path, locuteur_folder)
    locuteur_names.append(locuteur_folder)
    locuteur_max_values = []

    for file_name in os.listdir(locuteur_folder_path):
        if file_name.endswith(".TextGrid"):
            file_path = os.path.join(locuteur_folder_path, file_name)

            windows = extract_label_windows(file_path)
            
            
            label_a_windows = [window for window in windows if 'u' in window[2] or 'i' in window[2] or 'a' in window[2]]
            print(locuteur_folder,file_name, label_a_windows)

            locuteur_labels.extend(label_a_windows)
            
            

            # Retrieve maximum value for each window
            values_file_path = os.path.splitext(file_path)[0] + ".txt"
            if os.path.exists(values_file_path):
                values = np.loadtxt(values_file_path)  # Assuming the values are stored in a text file
                for window in label_a_windows:
                    xmin, xmax, _ = window
                    max_index, max_value = find_maxima_in_interval(values, xmin, xmax)
                    locuteur_max_values.append(max_value)
                    print(locuteur_folder,file_name, max_value)
            else:
                print(f"No values file found for {file_name}")

    locuteur_data.append(locuteur_max_values)


# Créer un DataFrame à partir des données collectées
df = pd.DataFrame(locuteur_data).T
df.columns = locuteur_names

# Calculer la moyenne pour chaque locuteur
means = df.mean()

# Définir les propriétés de la barre de moyenne
meanprops = {'color': 'black', 'linestyle': '-'}

# Afficher la boîte à moustaches en utilisant la moyenne comme barre centrale et masquer la barre médiane
ax = sns.boxplot(data=df, showmeans=True, meanline=True, medianprops={'visible': False}, meanprops=meanprops)

plt.xlabel("Locuteur")
plt.ylabel("Valeur maximale des voyelles")
plt.title("Boîte à moustaches des valeurs maximales des voyelles par locuteur")

# Ajouter la valeur de la moyenne sur le graphique
for i, mean in enumerate(means):
    plt.text(i, mean, f"{mean:.2f}", ha='center', va='bottom', color='black', fontsize=8)

plt.show()
