import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from matplotlib.patches import Patch
from scipy.stats import linregress, pearsonr  # Importez la fonction linregress et pearsonr pour le coefficient de corrélation

data = pd.read_csv('modelv2_test.tsv', delimiter='\t')

data['new_measure'] = pd.to_numeric(data['new_measure'], errors='coerce')
data['Score'] = pd.to_numeric(data['Score'], errors='coerce')

data['locuteur'] = data['locuteur'].astype('category')
filtered_data = data[data['mot'].isin(["dedede","bababa","gogogo"])]

seuil_bon = 6
seuil_mauvais = 10

palette = {0: '#1f77b4', 1: '#ff7f0e', 2: '#9467bd'}

filtered_data['cat_score'] = pd.cut(filtered_data['Score'], bins=[-float('inf'), seuil_bon, seuil_mauvais, float('inf')],
                                    labels=[0, 1, 2])

# Créer un graphique de dispersion avec les points colorés en fonction du score du patient
scatterplot = sns.scatterplot(x='Score', y='new_measure', data=filtered_data, hue='cat_score', palette=palette)

# Tracer une ligne de régression
sns.regplot(x='Score', y='new_measure', data=filtered_data, scatter=False, color='black', ci=5)



# Calculer le coefficient de corrélation (r-value) entre 'Score' et 'new_measure'
correlation_coefficient, _ = pearsonr(filtered_data['Score'], filtered_data['new_measure'])

# Afficher le coefficient de corrélation sur le graphique
plt.text(0.5, 0.05, f'Coefficient de corrélation (r-value): {correlation_coefficient:.2f}', transform=scatterplot.transAxes, fontsize=12, color='black')

# Définir les noms des catégories de couleur pour la légende
legend_labels = {0: 'Troubles légers', 1: 'Troubles modérés', 2: 'Troubles sévères'}

# Créer une légende pour les seuils de classification avec les couleurs correspondantes
legend_elements = [Patch(facecolor=palette[key], label=label) for key, label in legend_labels.items()]

# Ajouter la légende avec les couleurs correspondantes
plt.legend(handles=legend_elements, title='Score')
plt.title("Graphique courbe modulation dedede,bababa,gogogo en fonction de new_measure")

# Afficher le graphique
plt.show()
