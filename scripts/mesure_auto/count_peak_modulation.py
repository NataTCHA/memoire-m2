import os
import numpy as np
from scipy.signal import argrelextrema
import tgt
from scipy.signal import find_peaks
import numpy as np
from scipy.signal import find_peaks

def find_maxima_in_interval(interval_values, time, min_distance=8):
    # Trouver les indices des maxima avec les critères donnés
    maxima_indices, _ = find_peaks(interval_values, distance=min_distance, prominence=0.5)
    
    # Sélectionner les temps et les valeurs des maxima
    maxima_times = time[maxima_indices]
    maxima_values = interval_values[maxima_indices]
    
    # Calculer la moyenne des valeurs maximales, excluant le premier et le dernier maximum
    if len(maxima_values) > 2:  # S'assurer qu'il y a plus de deux maxima pour exclure le premier et le dernier
        avg_max = np.mean(maxima_values[1:-1])
        selected_maxima_values = maxima_values[1:-1]  # Sélectionner les valeurs maximales entre 1 et -1
    else:
        avg_max = np.mean(maxima_values)  # Utiliser toutes les valeurs si moins de 3 maxima sont trouvés
        selected_maxima_values = maxima_values

    return len(maxima_indices), selected_maxima_values, avg_max


def find_minima_in_interval(interval_values, time, min_distance=10):
    minima_indices, _ = find_peaks(-interval_values, distance=min_distance, prominence=1.5)
    minima_times = time[minima_indices]
    minima_values = interval_values[minima_indices]
    avg_min=np.mean(minima_values)
    return len(minima_indices), minima_times,avg_min

def calculate_distances(times):
    distances = np.diff(times)
    return distances

def mesure_auto(file_path, time, values):
    text_grid = tgt.io.read_textgrid(file_path)
    tier_names = ["words"]
    tier = None
    
    for t in text_grid.tiers:
        if t.name in tier_names:
            tier = t
            break
    
    pic_count = 0
    minima_count = 0
    maxima_times = []
    minima_times = []
    minima_distances = []
    mfcc_values_in_interval = []

    if tier is not None and len(tier.intervals) > 0:
        interval = tier.intervals[0]  # Prendre seulement le premier intervalle
        start_time_maxima = interval.start_time 
        end_time_maxima = interval.end_time

        start_time_minima = interval.start_time
        end_time_minima = interval.end_time

        indices_maxima = np.where((time >= start_time_maxima) & (time <= end_time_maxima))[0]
        indices_minima = np.where((time >= start_time_minima) & (time <= end_time_minima))[0]

        interval_values_maxima = values[indices_maxima]
        interval_values_minima = values[indices_minima]

        maxima_count_interval, maxima_times_interval, avg_max = find_maxima_in_interval(interval_values_maxima, time[indices_maxima])
        pic_count += maxima_count_interval
        maxima_times.extend(maxima_times_interval)

        minima_count_interval, minima_times_interval, avg_min = find_minima_in_interval(interval_values_minima, time[indices_minima])
        minima_count += minima_count_interval
        minima_times.extend(minima_times_interval)
        #event_dur
        minima_distances_interval = calculate_distances(minima_times_interval)
        minima_distances.extend(minima_distances_interval)

    minima_distances = np.array(minima_distances)
    event_dur_std_dev = np.std(minima_distances) if len(minima_distances) > 0 else 0
    event_dur_variance = np.var(minima_distances) if len(minima_distances) > 0 else 0
    event_dur_mean = np.mean(minima_distances) if len(minima_distances) > 0 else 0
    average_values = 0
    std_deviation = 0
    
    if minima_times:  
        first_minimum = minima_times[0]
        last_minimum = minima_times[-1]
        first_time = text_grid.start_time + first_minimum / 200  
        last_time = text_grid.start_time + last_minimum / 200  
        durationglob = last_time - first_time
        first_minimum_index = np.where(time == first_minimum)[0][0]
        last_minimum_index = np.where(time == last_minimum)[0][0]

        # Calcul de la moyenne des valeurs entre le premier et le dernier minimum
        average_values = np.mean(values[first_minimum_index:last_minimum_index + 1])
        valeur = np.array(values[first_minimum_index:last_minimum_index + 1])

        std_deviation = np.std(values[first_minimum_index:last_minimum_index + 1])

        print(valeur)
    
    return pic_count, minima_count, maxima_times, minima_times, minima_distances, average_values, std_deviation, event_dur_std_dev, event_dur_variance, event_dur_mean, avg_min, avg_max, valeur

directory_path = "CTRL/glides"
output_file_path = "CTRL_mod2.tsv"

headers = ["locuteur", "mot", "fichier","repetition", "Nombre de pics", "Nombre de minima", "valeurs des maxima", "valeurs des minima", "Distances entre minima successifs", "meanch", "std_modulation", "Ecart type Event Dur", "Varco Event Dur", "event-dur","avg_min","avg_max","valeursmincons"]

with open(output_file_path, 'w') as output_file:
    output_file.write("\t".join(headers) + "\n")

    for dirpath, dirnames, filenames in os.walk(directory_path):
        for filename in filenames:
            print(filename)
            if filename.endswith(".TextGrid"):
                text_grid_file_path = os.path.join(dirpath, filename)
                mfcc_filename = os.path.splitext(text_grid_file_path)[0] + '.txt'
                
                if os.path.exists(mfcc_filename):
                    with open(mfcc_filename, 'r') as f:
                        valeurs_mfcc = [float(line.strip()) for line in f]

                    values = np.array(valeurs_mfcc)
                    time = np.linspace(0, len(values) / 200, len(values))
                    pic_count, minima_count, maxima_times, minima_times, minima_distances, mfcc_average, mfcc_std_dev, event_dur_std_dev, event_dur_variance, event_dur_mean,avg_min,avg_max,valeursmincons = mesure_auto(text_grid_file_path, time, values)
                    filename_parts = filename.split("_")
                    fichier_parts = filename.split("_")[-2].split(".")[0].split("_")

                    if "ModuleDiadoco" in filename_parts:
                        index_module_diadoco = filename_parts.index("ModuleDiadoco")
                        module_diadoco_word = filename_parts[index_module_diadoco + 1] if index_module_diadoco + 1 < len(filename_parts) else ""
                        locuteur = "_".join(filename_parts[0:3]) if len(filename_parts) >= 3 else ""
                        fichier = module_diadoco_word
                        mots = module_diadoco_word
                        repetition = 1 

                    elif any(substring in filename for substring in ["aj", "uj", "wi"]):
                        substring = next(sub for sub in ["aj", "uj", "wi"] if sub in filename)
                        index_substring = filename.index(substring)
                        repetition = int(filename[index_substring + 2])
                        mots = substring * 3
                        fichier = substring+str(repetition)
                        locuteur = "_".join(filename_parts[0:3]) if len(filename_parts) >= 3 else ""

                    else:
                        locuteur = "_".join(filename_parts[0:3]) if len(filename_parts) >= 3 else ""
                        fichier_parts = filename_parts[-2].split(".")[0].split("_")
                        fichier = fichier_parts[0] if len(fichier_parts) >= 1 else ""
                        mots = fichier_parts[0] if len(fichier_parts) >= 1 else ""
                        repetition = 1 
                    
                    fichier = fichier.replace(".TextGrid", "")
                    mots = mots.replace(".TextGrid", "")

                    output_row = [locuteur, mots, fichier,repetition, pic_count, minima_count, ";".join(map(str, maxima_times)), ";".join(map(str, minima_times)), ";".join(map(str, minima_distances)), mfcc_average, mfcc_std_dev, event_dur_std_dev, event_dur_variance, event_dur_mean,avg_min,avg_max,";".join(map(str, valeursmincons))]
                    output_file.write("\t".join(map(str, output_row)) + "\n")