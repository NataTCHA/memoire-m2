import tgt

def extract_label_windows(file_path, tier_name="phon"):
    text_grid = tgt.io.read_textgrid(file_path)
    tier = None

    for t in text_grid.tiers:
        if t.name == tier_name:
            tier = t
            break

    label_windows = []
    if tier is not None:
        previous_label = None
        
        for interval in tier.intervals:
            start_time = interval.start_time
            end_time = interval.end_time

            if previous_label is None:  # Premier label
                xmin = start_time - 0.50
                xmax = (start_time + end_time) / 2
                labels = [interval.text]
                label_windows.append((xmin, xmax, labels))
            else:  # Label suivant
                xmin = (previous_label.start_time + previous_label.end_time) / 2
                xmax = (start_time + end_time) / 2
                labels = [interval.text]
                label_windows.append((xmin, xmax, labels))

            previous_label = interval

        if previous_label is not None:  # Dernier label
            start_time = previous_label.start_time
            end_time = previous_label.end_time
            xmin = (start_time + end_time) / 2
            xmax = end_time + 0.10
            labels = []
            label_windows.append((xmin, xmax, labels))

    return label_windows

# Exemple d'utilisation
file_path = "SCA_F_AB20_2020_11_11_ModuleTransitions_aj1.TextGrid"
windows = extract_label_windows(file_path)

for window in windows:
    xmin, xmax, labels = window
    print(f"Window: xmin={xmin}, xmax={xmax}")
    print(f"Labels: {labels}")
    print()
