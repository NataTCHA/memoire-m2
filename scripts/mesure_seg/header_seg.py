import numpy as np
import os

def parcourir_arborescence_dossier(dossier):
    tsv_rows = []
    
    for dossier_actuel, sous_dossiers, fichiers in os.walk(dossier):
        for fichier in fichiers:
            if fichier.endswith(".txt"):
                chemin_fichier = os.path.join(dossier_actuel, fichier)
                filename_parts = fichier.split("_")
                
                if "ModuleDiadoco" in filename_parts:
                    # Handle ModuleDiadoco case as before
                    index_module_diadoco = filename_parts.index("ModuleDiadoco")
                    
                    # Check if there is a word after the underscore following "ModuleDiadoco"
                    if index_module_diadoco + 1 < len(filename_parts):
                        module_diadoco_word = filename_parts[index_module_diadoco + 1]
                    else:
                        module_diadoco_word = ""

                    locuteur = "_".join(filename_parts[0:3]) if len(filename_parts) >= 3 else ""
                    fichier = module_diadoco_word
                    mot = module_diadoco_word
                    repetition = 1 

                elif any(substring in fichier for substring in ["aj", "uj", "wi"]):
                    # Handle "aj," "uj," or "wi" case
                    substring = next(sub for sub in ["aj", "uj", "wi"] if sub in fichier)
                    index_substring = fichier.index(substring)
                    repetition = int(fichier[index_substring + 2])
                    mot = substring * 3
                    fichier = substring

                    # Construct locuteur based on your requirements
                    locuteur = "_".join(filename_parts[0:3]) if len(filename_parts) >= 3 else ""

                else:
                    # Handle the general case as before
                    locuteur = "_".join(filename_parts[0:3]) if len(filename_parts) >= 3 else ""
                    fichier_parts = filename_parts[-2].split(".")[0].split("_")
                    fichier = fichier_parts[0] if len(fichier_parts) >= 1 else ""
                    mot = fichier_parts[0] if len(fichier_parts) >= 1 else ""
                    repetition = 1 

                # Remove the .txt extension from the fichier and mot fields
                fichier = fichier.replace(".txt", "")
                mot = mot.replace(".txt", "")

                tsv_row = [locuteur, fichier, mot, repetition]
                tsv_rows.append(tsv_row)

    headers = ["locuteur", "fichier", "mot", "repetition"]
    with open("CTRL_glides_seg.tsv", "w") as f:
        f.write("\t".join(headers) + "\n")
        for row in tsv_rows:
            row_str = "\t".join(map(str, row))
            f.write(row_str + "\n")

# Example usage
dossier_principal = "CTRL/glides"
parcourir_arborescence_dossier(dossier_principal)
