#fonction sans utiliser la segmentation qui prend le top7 des maximums
import numpy as np
from scipy.signal import argrelextrema
import matplotlib.pyplot as plt
from scipy.misc import electrocardiogram
from scipy.signal import find_peaks

with open("SCA_H_AB04_2017_01_09_ModuleTransitions_aj1.txt", 'r') as f:
        valeurs_modulation = [float(line.strip()) for line in f]

valeurs = np.array(valeurs_modulation)



max_ind = argrelextrema(valeurs, np.greater, order=1)


max_vals = valeurs[max_ind]


sorted_max_vals = np.sort(max_vals)[::-1]


top_max_vals = sorted_max_vals[:7]

print("Les 7 plus grands pics :", top_max_vals)




plt.plot(valeurs)
plt.xlabel('Échantillons')
plt.ylabel('Amplitude')
plt.title('Électrocardiogramme')


peaks, _ = find_peaks(valeurs, height=0)


plt.plot(peaks, valeurs[peaks], "x")

plt.grid(True)
plt.show()



#autre solution plus utilisable pour la suite qui prend en compte la segmentation

import numpy as np
from scipy.signal import argrelextrema

def find_maxima_in_interval(values, start_time, end_time):
    # Calculer la durée totale et l'intervalle d'échantillonnage
    duration = len(values) / fs
    time = np.linspace(0, duration, len(values))

    # Trouver les indices correspondant à l'intervalle de temps spécifié
    indices = np.where((time >= start_time) & (time <= end_time))[0]
    interval_values = values[indices]

    # Trouver les indices des maximums dans l'intervalle de valeurs
    maxima_indices = argrelextrema(interval_values, np.greater)[0]

    # Trouver l'indice du maximum le plus grand
    if len(maxima_indices) > 1:
        max_index = indices[maxima_indices[np.argmax(interval_values[maxima_indices])]]
        max_value = interval_values[maxima_indices[np.argmax(interval_values[maxima_indices])]]
        return max_index, max_value
    elif len(maxima_indices) == 1:
        max_index = indices[maxima_indices[0]]
        max_value = interval_values[maxima_indices[0]]
        return max_index, max_value
    else:
        average = np.mean(interval_values)

        return None, average