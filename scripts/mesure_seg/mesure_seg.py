import tgt
import numpy as np
from scipy.signal import argrelextrema
import os
import csv
from minimums_test import posdyn
from coordtemp import extract_compteur
def find_minima_in_interval(values, start_time, end_time):
    fs = 200
    duration = len(values) / fs
    time = np.linspace(0, duration, len(values))

    indices = np.where((time >= start_time) & (time <= end_time))[0]
    interval_values = values[indices]

    minima_indices = argrelextrema(interval_values, np.less)[0]

    if len(minima_indices) > 0:
        min_indices = indices[minima_indices]
        min_values = interval_values[minima_indices]
        print(len(minima_indices),"mini")
        return min_indices, min_values
    
    else:
        # Aucun minimum trouvé dans l'intervalle, recherche de 50 ms à droite
        right_end_time = end_time + 0.050
        right_indices = np.where((time > end_time) & (time <= right_end_time))[0]
        right_interval_values = values[right_indices]

        right_minima_indices = argrelextrema(right_interval_values, np.less)[0]

        if len(right_minima_indices) > 0:
            right_min_indices = right_indices[right_minima_indices]
            right_min_values = right_interval_values[right_minima_indices]
            return right_min_indices, right_min_values
        else:
            return None, None


def extract_label_windows2(file_path, tier_name="phones"):
    textgrid = tgt.io.read_textgrid(file_path)
    tier = None

    for t in textgrid.tiers:
        if t.name == tier_name:
            tier = t
            break

    label_windows = []
    if tier is not None:
        for interval in tier.intervals:
            start_time = interval.start_time
            end_time = interval.end_time
            label_windows.append((start_time, end_time))


    return label_windows

def calculate_duration_between_minima(textgrid_file, values_file):
    textgrid = tgt.read_textgrid(textgrid_file)

    with open(values_file, 'r') as f:
        valeurs_modulation = [float(line.strip()) for line in f]

    valeurs = np.array(valeurs_modulation)
    label_windows = extract_label_windows2(textgrid_file)
    all_minimas = []
    durationglob=0
    average_values=0
    meanch=0
    std_deviation=0
    minavg=[]
    for window in label_windows:
        start_time, end_time = window
        min_indices, min_values = find_minima_in_interval(valeurs, start_time, end_time)

        if min_values is not None:
            min_index = min_indices[np.argmin(min_values)]
            min_value = min_values[np.argmin(min_values)]
            all_minimas.append((min_index, min_value))
            
    min_values = [min_value for _, min_value in all_minimas]
    average_min = np.mean(min_values)
    sorted_minimas = sorted(all_minimas, key=lambda x: x[1])

    print("bidddddbi",average_min)
    smallest_minimas = sorted_minimas[:2]


    nombre_minimums=len(all_minimas)
    # Calcul de la durée entre le premier et le dernier minimum
    if all_minimas:
        first_minimum = all_minimas[0][0]
        last_minimum = all_minimas[-1][0]
        first_time = textgrid.start_time + first_minimum / 200  # Conversion en secondes
        last_time = textgrid.start_time + last_minimum / 200  # Conversion en secondes
        durationglob = last_time - first_time
        first_minimum_index = all_minimas[0][0]
        last_minimum_index = all_minimas[-1][0]

        # Calcul de la moyenne des valeurs entre le premier et le dernier minimum
        average_values = np.mean(valeurs[first_minimum_index:last_minimum_index+1])
        valeur=np.array(valeurs[first_minimum_index:last_minimum_index+1])
        print(valeur)
        std_deviation = np.std(valeurs[first_minimum_index:last_minimum_index+1])

    eventdur=[]
    mean_eventdur=0
    #event_dur

    total_weighted_sum = 0
    total_duration = 0
    for i in range(len(all_minimas)-1):
        
        minimum1=all_minimas[i][0]
        minimum2=all_minimas[i+1][0]

        max_value=0
        # Obtenez les indices des maximums entre minimum1 et minimum2
        maxima_indices = argrelextrema(valeurs[minimum1:minimum2+1], np.greater)[0]

        if len(maxima_indices) > 0:
            # Récupérez les valeurs de maximums correspondantes
            maxima_values = valeurs[minimum1 + maxima_indices]

            # Trouvez l'indice du maximum le plus élevé
            max_index = minimum1 + maxima_indices[np.argmax(maxima_values)]
            max_value = maxima_values[np.argmax(maxima_values)]
            
            print(f"Maximum entre minimum {i} et {i+1}: Index={max_index}, Valeur={max_value}")
        else:
            print(f"Aucun maximum entre minimum {i} et {i+1}")        
        duree_minimum1 = textgrid.start_time + minimum1 / 200  # Conversion en secondes
        duree_minimum2 = textgrid.start_time + minimum2 / 200  # Conversion en secondes
        duration = duree_minimum2 - duree_minimum1
        weighted_sum = max_value
        total_weighted_sum += weighted_sum
        total_duration += duration
        eventdur.append(duration)
        mean_eventdur=np.mean(eventdur)
        std_eventdur=np.std(eventdur)
    print("event_duur",eventdur)
    return(durationglob,mean_eventdur,average_values,std_deviation,nombre_minimums,std_eventdur,average_min)

def find_maxima_in_interval(values, start_time, end_time, counter):
    fs = 200
    duration = len(values) / fs
    time = np.linspace(0, duration, len(values))

    indices = np.where((time >= start_time) & (time <= end_time))[0]
    interval_values = values[indices]

    maxima_indices = argrelextrema(interval_values, np.greater)[0]

    if len(maxima_indices) > 1:
        max_index = indices[maxima_indices[np.argmax(interval_values[maxima_indices])]]
        max_value = interval_values[maxima_indices[np.argmax(interval_values[maxima_indices])]]
        print(max_value)
        return max_index, max_value, counter
    elif len(maxima_indices) == 1:
        max_index = indices[maxima_indices[0]]
        max_value = interval_values[maxima_indices[0]]
        print(max_value)
        return max_index, max_value, counter
    else:
        average = np.mean(interval_values)
        counter += 1
        print(average)
        return None, average, counter

def extract_label_windows(file_path, tier_name="phones"):
    text_grid = tgt.io.read_textgrid(file_path)
    tier = None
    
    for t in text_grid.tiers:
        if t.name == tier_name:
            tier = t
            break

    label_windows = []
    if tier is not None and len(tier.intervals) > 0:
        previous_label = None
        for interval in tier.intervals:
            start_time = interval.start_time
            end_time = interval.end_time
            label = interval.text

            if previous_label is None:  # Premier label
                xmin = start_time - 0.25
                xmax = (start_time + end_time) / 2
                labels = [label]
                label_windows.append((xmin, xmax, labels))
            else:  # Label suivant
                xmin = (previous_label.start_time + previous_label.end_time) / 2
                xmax = (start_time + end_time) / 2
                labels = [label]
                label_windows.append((xmin, xmax, labels))

            previous_label = interval

        if previous_label is not None:  # Dernier label
            start_time = previous_label.start_time
            end_time = previous_label.end_time
            xmin = (start_time + end_time) / 2
            xmax = end_time + 0.10
            labels = []
            label_windows.append((xmin, xmax, labels))

    print("fenetre",label_windows)  
    return label_windows
def parcourir_arborescence_dossier(dossier, fichier_tsv):
    tsv_rows = []
    with open(fichier_tsv, 'r') as tsv_file:
        reader = csv.reader(tsv_file, delimiter='\t')
        tsv_rows = list(reader)

    header = tsv_rows[0]
    header.append("avg_seg")
    header.append("compteur_solution")
    header.append("event-dur")
    header.append("globdur")
    header.append("meanch")
    header.append("std_meanch")
    header.append("minimums") 
    header.append("negdyn")
    header.append("posdyn")
    header.append("syllabe")
    header.append("std-eventdur")
    header.append("avg_minimum")
    new_column_globdur=[]
    new_column_eventdur=[]
    new_column_values_avg_seg = []
    new_column_values_compteur_solution = []
    new_column_mean=[]
    new_column_std=[]
    new_column_posdyn=[]
    new_column_negdyn=[]
    new_column_syllabe=[]
    new_column_newmeasure=[]
    new_column_avg_min=[]
    

    new_column_mini=[]
    for dossier_actuel, sous_dossiers, fichiers in os.walk(dossier):
        for fichier in fichiers:
            if fichier.endswith(".txt"):
                chemin_fichier = os.path.join(dossier_actuel, fichier)
                fichier_mfcc = fichier.replace(".txt", ".TextGrid")

                
                
                with open(chemin_fichier, 'r') as f:
                    valeurs_mfcc = [float(line.strip()) for line in f]

                valeurs = np.array(valeurs_mfcc)

                file_path = os.path.join(dossier_actuel, fichier_mfcc)
                print(file_path)
                if os.path.isfile(file_path):
                    globdur, eventdur, meanch, std , mini,std_eventdur,avg_min = calculate_duration_between_minima(file_path, chemin_fichier)
                    negdyn,posdyng= posdyn(file_path, chemin_fichier)
                    syllabes=extract_compteur(file_path)
                    new_column_globdur.append(globdur)
                    new_column_eventdur.append(eventdur)
                    new_column_mean.append(meanch)
                    new_column_std.append(std)
                    new_column_mini.append(mini)
                    new_column_negdyn.append(negdyn)
                    new_column_posdyn.append(posdyng)
                    new_column_syllabe.append(syllabes)
                    new_column_newmeasure.append(std_eventdur)
                    new_column_avg_min.append(avg_min)
                    
                else:
                    new_column_globdur.append("none")
                    new_column_eventdur.append("none")
                    new_column_mean.append("none")
                    new_column_std.append("none")
                    new_column_mini.append("none")
                    new_column_negdyn.append("none")
                    new_column_posdyn.append("none")
                    new_column_syllabe.append("none")
                    new_column_newmeasure.append("none")
                    new_column_avg_min.append("none")
                compteur = 0  # Initialiser le compteur pour chaque fichier


                if os.path.isfile(file_path):
                    
                    windows = extract_label_windows(file_path)


                    intervals = list(extract_label_windows(file_path, tier_name="phones")) #prend la segmentation pour moyenne xpics

                    L = []
                    for interval in intervals:
                        start_time, end_time, label = interval

                        try:
                            maxima_indices, maxima_values, compteur = find_maxima_in_interval(valeurs, start_time, end_time, compteur)
                            if maxima_values is None:
                                average_value = "none"
                            else:
                                average_value = np.mean(maxima_values)

                            L.append(average_value)
                        except FileNotFoundError:
                            L.append("none")

                    val_moyenne_7pic = np.mean(L)
                    new_column_values_avg_seg.append(val_moyenne_7pic)
                    new_column_values_compteur_solution.append(compteur)

                else:
                    new_column_values_avg_seg.append("none")
                    new_column_values_compteur_solution.append("none")

    for i, value_avg_seg in enumerate(new_column_values_avg_seg):
        tsv_rows[i+1].append(value_avg_seg)
    for i, value_compteur_solution in enumerate(new_column_values_compteur_solution):
        tsv_rows[i+1].append(value_compteur_solution)
    for i, valuer_event in enumerate(new_column_eventdur):    
        tsv_rows[i+1].append(valuer_event)
    for i, globdure in enumerate(new_column_globdur):
        tsv_rows[i+1].append(globdure)
    for i, mean_val in enumerate(new_column_mean):
        tsv_rows[i+1].append(mean_val)
    for i, std in enumerate(new_column_std):
        tsv_rows[i+1].append(std) 
    for i, mini in enumerate(new_column_mini):
        tsv_rows[i+1].append(mini)
    for i, negdyn in enumerate(new_column_negdyn):
        tsv_rows[i+1].append(negdyn)
    for i, posdyng in enumerate(new_column_posdyn):
        tsv_rows[i+1].append(posdyng)
    for i, nbsyllabes in enumerate(new_column_syllabe):
        tsv_rows[i+1].append(nbsyllabes)
    for i, newmeasures in enumerate(new_column_newmeasure):
        tsv_rows[i+1].append(newmeasures)
    for i,avg_min in enumerate(new_column_avg_min):
        tsv_rows[i+1].append(avg_min)



    with open(fichier_tsv, 'w', newline='') as tsv_file:
        writer = csv.writer(tsv_file, delimiter='\t')
        writer.writerows(tsv_rows)
parcourir_arborescence_dossier("CTRL\glides", "CTRL_glides_seg.tsv")