import tgt
import numpy as np
from scipy.signal import argrelextrema
import numpy as np
def find_minima_in_interval(values, start_time, end_time):
    fs = 200
    duration = len(values) / fs
    time = np.linspace(0, duration, len(values))

    indices = np.where((time >= start_time) & (time <= end_time))[0]
    interval_values = values[indices]

    minima_indices = argrelextrema(interval_values, np.less)[0]

    if len(minima_indices) > 0:
        min_indices = indices[minima_indices]
        min_values = interval_values[minima_indices]
        return min_indices, min_values
    else:
        # Aucun minimum trouvé dans l'intervalle, recherche de 50 ms à droite
        right_end_time = end_time + 0.050
        right_indices = np.where((time > end_time) & (time <= right_end_time))[0]
        right_interval_values = values[right_indices]

        right_minima_indices = argrelextrema(right_interval_values, np.less)[0]

        if len(right_minima_indices) > 0:
            right_min_indices = right_indices[right_minima_indices]
            right_min_values = right_interval_values[right_minima_indices]
            return right_min_indices, right_min_values
        else:
            return None, None


def extract_label_windows(file_path, tier_name="phon"):
    textgrid = tgt.io.read_textgrid(file_path)
    tier = None

    for t in textgrid.tiers:
        if t.name == tier_name:
            tier = t
            break

    label_windows = []
    if tier is not None:
        for interval in tier.intervals:
            start_time = interval.start_time
            end_time = interval.end_time
            label_windows.append((start_time, end_time))

    return label_windows

def calculate_duration_between_minima(textgrid_file, values_file):
    textgrid = tgt.read_textgrid(textgrid_file)

    with open(values_file, 'r') as f:
        valeurs_modulation = [float(line.strip()) for line in f]

    valeurs = np.array(valeurs_modulation)

    label_windows = extract_label_windows(textgrid_file)
    all_minimas = []
    for window in label_windows:
        start_time, end_time = window
        min_indices, min_values = find_minima_in_interval(valeurs, start_time, end_time)

        if min_values is not None:
            min_index = min_indices[np.argmin(min_values)]
            min_value = min_values[np.argmin(min_values)]
            all_minimas.append((min_index, min_value))

    sorted_minimas = sorted(all_minimas, key=lambda x: x[1])
    smallest_minimas = sorted_minimas[:2]



    # Calcul de la durée entre le premier et le dernier minimum
    if all_minimas:
        first_minimum = all_minimas[0][0]
        last_minimum = all_minimas[-1][0]
        first_time = textgrid.start_time + first_minimum / 200  # Conversion en secondes
        last_time = textgrid.start_time + last_minimum / 200  # Conversion en secondes
        durationglob = last_time - first_time

    eventdur=[]
    for i in range(len(all_minimas)-1):
        minimum1=all_minimas[i][0]
        minimum2=all_minimas[i+1][0]
        duree_minimum1 = textgrid.start_time + minimum1 / 200  # Conversion en secondes
        duree_minimum2 = textgrid.start_time + minimum2 / 200  # Conversion en secondes
        duration = duree_minimum2 - duree_minimum1
        eventdur.append(duration)
        
        mean_eventdur=np.mean(eventdur)
    print(mean_eventdur)
    print(durationglob)
        
    return(durationglob,mean_eventdur)
textgrid_file = "SCA_H_AB02_2017_01_01_ModuleTransitions_wi1.TextGrid"
values_file = "SCA_H_AB02_2017_01_01_ModuleTransitions_wi1.txt"
calculate_duration_between_minima(textgrid_file, values_file)
