import tgt
import numpy as np
from scipy.signal import argrelextrema
from collections import defaultdict
import statistics
def extract_label_windows(file_path, tier_name="phon"):
    text_grid = tgt.io.read_textgrid(file_path)
    tier = None
    
    for t in text_grid.tiers:
        if t.name == tier_name:
            tier = t
            break

    label_windows = []
    if tier is not None and len(tier.intervals) > 0:
        for interval in tier.intervals:
            start_time = interval.start_time
            end_time = interval.end_time
            label = interval.text

            xmin = start_time - 0.10
            xmax = (start_time + end_time) / 2

            label_windows.append((xmin, xmax, label))

    return label_windows

def extract_label_windows2(file_path, tier_name="phon"):
    textgrid = tgt.io.read_textgrid(file_path)
    tier = None

    for t in textgrid.tiers:
        if t.name == tier_name:
            tier = t
            break

    label_windows = []
    if tier is not None:
        for interval in tier.intervals:
            start_time = interval.start_time
            end_time = interval.end_time
            label_windows.append((start_time, end_time, None))

    return label_windows


def find_maxima_in_interval(values, start_time, end_time):
    # Calculer la durée totale et l'intervalle d'échantillonnage
    duration = len(values) / 200
    time = np.linspace(0, duration, len(values))

    # Trouver les indices correspondant à l'intervalle de temps spécifié
    indices = np.where((time >= start_time) & (time <= end_time))[0]
    interval_values = values[indices]

    # Trouver les indices des maximums dans l'intervalle de valeurs
    maxima_indices = argrelextrema(interval_values, np.greater)[0]

    # Trouver l'indice du maximum le plus grand
    if len(maxima_indices) > 1:
        max_index = indices[maxima_indices[np.argmax(interval_values[maxima_indices])]]
        max_value = interval_values[maxima_indices[np.argmax(interval_values[maxima_indices])]]
        print(max_index)
        return max_index, max_value
    elif len(maxima_indices) == 1:
        max_index = indices[maxima_indices[0]]
        max_value = interval_values[maxima_indices[0]]

        return max_index, max_value
    else:
        average = np.mean(interval_values)
        average_index = np.mean(indices)

        return average_index, average

def find_minima_in_interval(values, start_time, end_time):
    fs = 200
    duration = len(values) / fs
    time = np.linspace(0, duration, len(values))

    indices = np.where((time >= start_time) & (time <= end_time))[0]
    interval_values = values[indices]

    minima_indices = argrelextrema(interval_values, np.less)[0]

    if len(minima_indices) > 0:
        min_indices = indices[minima_indices]
        min_values = interval_values[minima_indices]
        return min_indices, min_values
    else:

        right_end_time = end_time + 0.050
        right_indices = np.where((time > end_time) & (time <= right_end_time))[0]
        right_interval_values = values[right_indices]

        right_minima_indices = argrelextrema(right_interval_values, np.less)[0]

        if len(right_minima_indices) > 0:
            right_min_indices = right_indices[right_minima_indices]
            right_min_values = right_interval_values[right_minima_indices]
            return right_min_indices, right_min_values
        else:
            return None, None


def posdyn(textgrid_file, values_file):

    

    with open(values_file, 'r') as f:
        valeurs_modulation = [float(line.strip()) for line in f]

    valeurs = np.array(valeurs_modulation)

    label_windows = extract_label_windows(textgrid_file)

    all_minimas = []
    all_maximas = []
    label2 = extract_label_windows2(textgrid_file)

    unique_minimas = []
    for window2 in label2:
        start_time, end_time, label = window2
        min_indices, min_values = find_minima_in_interval(valeurs, start_time, end_time)
        if min_values is not None:
            min_index = min_indices[np.argmin(min_values)]
            min_value = min_values[np.argmin(min_values)]
            if (min_index, min_value) not in unique_minimas:
                unique_minimas.append((min_index, min_value))

    # Trouver les maxima uniques
    unique_maximas = []
    for window in label_windows:
        start_time, end_time, label = window
        max_index, max_value = find_maxima_in_interval(valeurs, start_time, end_time)
        
        if max_value is not None:  # Correction : Utiliser max_value au lieu de min_values
            
            if (max_index, max_value) not in unique_maximas and (max_index, max_value) not in all_maximas:
                unique_maximas.append((max_index, max_value))

    all_minimas += unique_minimas
    all_maximas += unique_maximas

    if all_maximas and len(all_maximas) > 1:
        all_maximas = all_maximas[1:]  # on retire le premier maximum on prendra celui après le premier minimum

    posdyng = []

    for i in range(len(all_minimas)):
        minimum_index, _ = all_minimas[i]
        
        # Recherche du premier maximum qui suit le minimum
        j = i
        while j < len(all_maximas):
            next_maximum_index, _ = all_maximas[j]

            
            if minimum_index < next_maximum_index:
                break
            
            j += 1
        
        if j < len(all_maximas):
            next_maximum_index, _ = all_maximas[j]
            duration_min_max = (next_maximum_index - minimum_index) / 200  # Conversion en secondes
            
            minimum_value = all_minimas[i][1]  
            maximum_value = all_maximas[j][1]  # Valeur du maximum correspondant
            print("pos",minimum_value,maximum_value, duration_min_max)
            posdyn = ((maximum_value - minimum_value) / duration_min_max)
            print(posdyn)
            posdyng.append(posdyn)

    posdynmean = np.mean(posdyng)
    #negdyn
   
  

    if all_minimas and len(all_minimas)>1:
        all_minimas=all_minimas[1:]
        
    last_min_index = -1
    last_max_index = -1
    negdyng = []  
    result_dict = {}

    for i in range(len(all_maximas)):
        maximum_index, _ = all_maximas[i]


        next_min_after_max = len(all_minimas)
        for j in range(len(all_minimas)):
            next_minimum_index, _ = all_minimas[j]
            if maximum_index < next_minimum_index and next_minimum_index > last_max_index:
                next_min_after_max = j

                break
        
        if next_min_after_max < len(all_minimas):
            next_minimum_index, _ = all_minimas[next_min_after_max]
            duration_max_min = (next_minimum_index - maximum_index) / 200

            maximum_value = all_maximas[i][1]
            minimum_value = all_minimas[next_min_after_max][1]
            print(duration_max_min)
            negdyn_val = (minimum_value - maximum_value) / duration_max_min
            negdyng.append(negdyn_val)
            print(maximum_value,minimum_value, duration_min_max)
            
            #suppression de paire utilisé en double
            if minimum_value not in result_dict:
                result_dict[minimum_value] = (maximum_value, negdyn_val)
            negdyn_values_list = [value[1] for value in result_dict.values()]


            last_min_index = next_minimum_index
            last_max_index = maximum_index
            
    

    average_negdyn_val = np.mean(negdyn_values_list)
    print("negdynavg=", average_negdyn_val,"`\n posdyavg = ",posdynmean)
    return average_negdyn_val,posdynmean
textgrid_file = "SCA_H_AB02_2017_01_01_ModuleTransitions_aj1.TextGrid"
values_file = "SCA_H_AB02_2017_01_01_ModuleTransitions_aj1.txt"
posdyn(textgrid_file, values_file)
