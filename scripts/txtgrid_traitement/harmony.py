import textgrid
import os
def extraire_tier(textgrid_obj, tier_name):

    for tier in textgrid_obj.tiers:
        if tier.name == tier_name:
            return tier
    return None

def modifier_tier(file_path, tier_name):

    textgrid_obj = textgrid.TextGrid.fromFile(file_path)

    tier = extraire_tier(textgrid_obj, tier_name)

    if tier is not None:
        for interval in tier.intervals:
            if interval.mark.startswith('aj'):
                interval.mark = 'ajajaj'
            elif interval.mark.startswith('uj'):
                interval.mark = 'ujujuj'
            elif interval.mark.startswith('wi'):
                interval.mark = 'wiwiwi'

        textgrid_obj.write(file_path)
        print("ok")
    else:
        print("pasok")

dossier_textgrid = "ctrl_glides_ok_cecile"

for fichier in os.listdir(dossier_textgrid):
    if fichier.endswith(".TextGrid"):
        chemin_fichier = os.path.join(dossier_textgrid, fichier)
        modifier_tier(chemin_fichier, "items")
